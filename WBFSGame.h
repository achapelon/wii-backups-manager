//
//  WBFSGame.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 21/04/10.
//  Copyright 2010 INSERM U556. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kGameTitleUnknown @"Unknown"

@interface WBFSGame : NSObject {
	NSString *_gameId;
	NSString *_name;
	NSNumber *_sizeGB;
	NSString *_format;
	NSString *_path;
	NSString *_region;
	NSDate	 *_releaseDate;
	NSString *_genre;
}

@property (nonatomic, retain) NSString *gameId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *sizeGB;
@property (nonatomic, retain) NSString *format;
@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *region;
@property (nonatomic, retain) NSDate   *releaseDate;
@property (nonatomic, retain) NSString *genre;

- (id)initWithFile:(NSString *)file;
- (id)initWithString:(NSString *)str;

- (void)parseString:(NSString *)str;

@end
