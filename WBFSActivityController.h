//
//  WBFSQueueProcessViewController.h
//
//  Created by Anthony Chapelon on 29/01/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <BWToolkitFramework/BWInsetTextField.h>

@class WBFSProcess;
@interface WBFSActivityController : NSWindowController <NSTableViewDataSource, NSTableViewDelegate> {
	NSTableView *processesTableView;
	NSMutableArray *processesList;
	IBOutlet NSTextField *statusText;

	NSUInteger numberOfTask;
	NSUInteger numberOfFinishedTask;
	double totalOfRunningProgress;
	BOOL taskFinishing;
}

@property (readonly) IBOutlet NSTableView *processesTableView;
@property (assign) NSMutableArray *processesList;

- (void)setup;
- (void)addProcessActivity:(WBFSProcess *)aProcess;
- (void)refresh;

- (IBAction)removeSelectedProcessesActivity:(id)sender;
- (IBAction)removeAllFinishedProcessesActivity:(id)sender;
- (void)removeProcessActivity:(NSDictionary *)process;

- (void)reloadProcessesActivity;
- (WBFSProcess *)nextProcessActivityToLaunch;

- (BOOL)isRunning;

- (void)updateDockIcon;
- (void)updateStatusText;

@end
