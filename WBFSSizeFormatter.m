//
//  WBFSSizeFormatter.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 16/03/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSSizeFormatter.h"


@implementation WBFSSizeFormatter

- (NSString *)stringForObjectValue:(id)anObject {
    return [NSString stringWithFormat:@"%.2f GB", [anObject  floatValue]];
}

- (BOOL)getObjectValue:(id *)obj forString:(NSString *)string errorDescription:(NSString  **)error {
	
    float floatResult;
    NSScanner *scanner;
    BOOL returnValue = NO;
	
    scanner = [NSScanner scannerWithString: string];
    if ([scanner scanFloat:&floatResult]) {
        returnValue = YES;
        if (obj)
            *obj = [NSNumber numberWithFloat:floatResult];
    } else {
        if (error)
            *error = NSLocalizedString(@"Couldn’t convert  to float", @"Error converting");
    }
    return returnValue;
}

- (NSAttributedString *)attributedStringForObjectValue:(id)anObject withDefaultAttributes:(NSDictionary *)attributes {
	return nil;
}

@end
