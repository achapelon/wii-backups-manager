//
//  WBFSManagedPlaylist.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 04/08/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kPlaylistEntityName		@"Playlist"
#define kPlaylistNameKey		@"name"
#define kPlaylistPredicateKey	@"predicate"
#define kPlaylistGamesKey		@"games"

@interface WBFSManagedPlaylist : NSManagedObject {
	NSImage *icon;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *predicate;
@property (nonatomic, retain) NSImage *icon;

+ (WBFSManagedPlaylist *)insertWithName:(NSString *)name;
- (WBFSManagedPlaylist *)initWithName:(NSString *)name;

- (NSSet *)games;
- (void)setGames:(NSSet *)set;
@end
