//
//  WBFSManagerWindow.m
//  Wii Backups Manager
//
//  Created by Anthony Chapelon on 11/01/11.
//  Copyright 2011 INSERM U556. All rights reserved.
//

#import "WBFSManagerWindow.h"
#import "WBFSManagerAppDelegate.h"

@implementation WBFSManagerWindow

- (void)flagsChanged:(NSEvent *)event
{	
	[super flagsChanged:event];
	
	WBFSManagerAppDelegate *app = [NSApp delegate];
	NSUInteger modifierFlags = [event modifierFlags];
	NSLog(@"modifierFlags: %lu", modifierFlags);
	if (modifierFlags & NSAlternateKeyMask) {
		[app.addPlaylistButton setAction:@selector(addSmartPlaylist:)];
		[app.addPlaylistButton setImage:[NSImage imageNamed:NSImageNameSmartBadgeTemplate]];
	} else {
		[app.addPlaylistButton setAction:@selector(addPlaylist:)];
		[app.addPlaylistButton setImage:[NSImage imageNamed:NSImageNameAddTemplate]];
	}
}

@end
