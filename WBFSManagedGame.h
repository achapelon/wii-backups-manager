//
//  WBFSManagedGame.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 25/03/10.
//  Copyright 2010. All rights reserved.
//

#import <Quartz/Quartz.h>
#import <Cocoa/Cocoa.h>

#define kGameEntityName		@"Game"
#define kGameIDKey			@"id"
#define kGameNameKey		@"name"
#define kGameFilePathKey	@"path"
#define kGameSizeGBKey		@"sizeGB"
#define kGameFormatKey		@"format"
#define kGameRatingKey		@"rating"
#define kGameRegionKey		@"region"
#define kGameReleaseDateKey	@"releaseDate"
#define kGameGenreKey	    @"genre"

#define kImageURLCoverFormat @"http://wiitdb.com/wiitdb/artwork/cover/%@/%@.png"
#define kImageURLCover3DFormat @"http://wiitdb.com/wiitdb/artwork/cover3D/%@/%@.png"
#define kImageURLCoverfullFormat @"http://wiitdb.com/wiitdb/artwork/coverfull/%@/%@.png"
#define kImageURLCoverfullHQFormat @"http://wiitdb.com/wiitdb/artwork/coverfullHQ/%@/%@.png"
#define kImageURLDiscFormat @"http://wiitdb.com/wiitdb/artwork/disc/%@/%@.png"

#define kGameTitleUnknown @"Unknown"

#define WBFSManagedGameDataType @"WBFSManagedGameDataType"

@class WBFSGame;
@interface WBFSManagedGame : NSManagedObject <QLPreviewItem> {
	NSImage *_imageCache;
	NSString *_imageUID;
}

@property (nonatomic, retain) NSString *gameId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *region;
@property (nonatomic, retain) NSNumber *sizeGB;
@property (nonatomic, retain) NSString *format;
@property (nonatomic, retain) NSDate   *releaseDate;
@property (nonatomic, retain) NSString *genre;
@property (nonatomic, retain) NSNumber *rating;
@property (nonatomic, retain) NSImage  *image;

+ (WBFSManagedGame *)insertGame:(WBFSGame *)game;
- (WBFSManagedGame *)insertGame:(WBFSGame *)game;

- (NSString *)localeForArtworks;
- (NSImage *)imageCover;
- (NSImage *)imageCover3D;
- (NSImage *)imageCoverfull;
- (NSImage *)imageCoverfullHQ;
- (NSImage *)imageDisc;
- (NSImage *)imageCoverForLocale:(NSString *)locale;
- (NSImage *)imageCover3DForLocale:(NSString *)locale;
- (NSImage *)imageCoverfullForLocale:(NSString *)locale;
- (NSImage *)imageCoverfullHQForLocale:(NSString *)locale;
- (NSImage *)imageDiscForLocale:(NSString *)locale;

- (void)clearCachedImage;

@end
