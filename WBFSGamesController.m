//
//  WBFSGamesController.m
//  Wii Backups Manager
//
//  Created by Anthony on 18/06/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WBFSGamesController.h"
#import "WBFSManagerAppDelegate.h"

@implementation WBFSGamesController

- (void)didChangeArrangementCriteria {
	[super didChangeArrangementCriteria];
	WBFSManagerAppDelegate *app = [NSApp delegate];
	[app.coverFlowView reloadData];
}

@end
