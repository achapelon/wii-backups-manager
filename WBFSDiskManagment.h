/*
 *  WBFSDiskManagment.h
 *  WBFS File GUI
 *
 *  Created by Anthony Chapelon on 07/03/10.
 *  Copyright 2010. All rights reserved.
 *
 */

#include <IOKit/IOBSD.h>
#include <IOKit/storage/IOMedia.h>

#include <DiskArbitration/DADisk.h>

#define kDADiskDescriptionUSBProtocolValue	CFSTR("USB")
#define kWBFSDiskPathKey					CFSTR("path")
#define kWBFSDiskNameKey					CFSTR("name")

void *WBFSGetDisks(void);
void WBFSSetupDisksNotification(void);
void WBFSDiskAppeared(DADiskRef disk, void *context);
void WBFSDiskDisappeared(DADiskRef disk, void *context);