//
//  WBFSManagedDevice.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 27/03/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kDeviceEntityName	@"Device"
#define kDevicePathKey		@"path"
#define kDeviceNameKey		@"name"
#define kDeviceFormatWiiKey	@"formatWii"
#define kDeviceTotalGBKey	@"totalGB"
#define kDeviceUsedGBKey	@"usedGB"
#define kDeviceFreeGBKey	@"freeGB"

@interface WBFSManagedDevice : NSManagedObject {
	NSImage *icon;
}

@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) BOOL formatWii;
@property (nonatomic, retain) NSNumber *totalGB;
@property (nonatomic, retain) NSNumber *usedGB;
@property (nonatomic, retain) NSNumber *freeGB;
@property (nonatomic, retain) NSImage *icon;

+ (WBFSManagedDevice *)insertWithPath:(NSString *)path forName:(NSString *)name;
- (WBFSManagedDevice *)initWithPath:(NSString *)path forName:(NSString *)name;

- (void)updateInfo;
- (void)updateInfoByUsingSpace:(float)space;
- (void)updateInfoByFreeingSpace:(float)space;

@end
