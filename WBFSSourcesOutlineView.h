//
//  WBFSSourcesOutlineView.h
//  Wii Manager
//
//  Created by Anthony on 11/06/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define WBFSSourcesDataType @"WBFSSourcesDataType"

@interface WBFSSourcesOutlineView : NSOutlineView {
	NSArray *draggedItems;
}

@property(nonatomic, retain) NSArray *draggedItems;

@end
