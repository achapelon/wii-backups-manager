//
//  WBFSProcessCell.m
//
//  Created by Anthony Chapelon on 29/01/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSProcessCell.h"

#define IMAGE_INSET 8.0
#define IMAGE_SIZE 32
#define ASPECT_RATIO 1
#define TITLE_FONT_SIZE 12
#define INFO_FONT_SIZE 10
#define INSET_FROM_IMAGE_TO_TEXT 4.0
#define INDICATOR_HEIGHT 12

@implementation WBFSProcessCell

@synthesize item;

- (NSProgressIndicator *)initProcessIndicator {
	NSProgressIndicator *aProgress = [[NSProgressIndicator alloc] init];
	[aProgress setIndeterminate:NO];
	[aProgress setStyle:NSProgressIndicatorBarStyle];
	[aProgress setControlSize:NSSmallControlSize];
	[aProgress sizeToFit];
	[aProgress setMinValue:0.0];
	[aProgress setMaxValue:100.0];
	
	return aProgress;
}

- (void)prepareCell {
	_cancelButtonCell = [[NSButtonCell alloc] init];
	[_cancelButtonCell setImage:[NSImage imageNamed:@"Activity_Stop.tif"]];
	[_cancelButtonCell setImageScaling:NSImageScaleProportionallyUpOrDown];
	[_cancelButtonCell setBackgroundStyle:NSBackgroundStyleLight];
	[_cancelButtonCell setTarget:self];
	[_cancelButtonCell setAction:@selector(cancelProcess:)];
}

- (void)cancelProcess:(id)sender {
	//[[[self process] task] terminate];
}

- (NSAttributedString *)info {
    return [_infoCell attributedStringValue];
}

- (void)setInfo:(NSString *)info {
    if (_infoCell == nil) {
        _infoCell = [[NSTextFieldCell alloc] init];
		[_infoCell setControlView:self.controlView];
    }
	[_infoCell setBackgroundStyle:self.backgroundStyle];
	//[_infoCell setBackgroundStyle:NSBackgroundStyleRaised];
	
	NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys: [NSColor blackColor], NSForegroundColorAttributeName, [NSFont systemFontOfSize:INFO_FONT_SIZE], NSFontAttributeName, nil];
	NSAttributedString *attrInfo = [[NSAttributedString alloc] initWithString:info attributes:textAttributes];
	[_infoCell setAttributedStringValue:attrInfo];
	
	[attrInfo release];
	//[textAttributes release];
}

- (NSImage *)image {
    return _imageCell.image;
}

- (void)setImage:(NSImage *)image {
    if (_imageCell == nil) {
        _imageCell = [[NSImageCell alloc] init];
        [_imageCell setControlView:self.controlView];
    }
	[_imageCell setBackgroundStyle:self.backgroundStyle];
	_imageCell.image = image;
}


- (NSProgressIndicator *)progressIndicator {
	return [item valueForKey:@"ProgressIndicator"];
	//return [[self objectValue] valueForKey:@"ProgressIndicator"];
}

- (WBFSProcess *)process {
	return [item valueForKey:@"Process"];
	//return [[self objectValue] valueForKey:@"Process"];
}

- (NSRect)_imageFrameForInteriorFrame:(NSRect)frame {
    NSRect result = frame;
	
	// Set the size
    result.size.height = IMAGE_SIZE;
    // Make the width match the aspect ratio based on the height
    result.size.width = ceil(result.size.height * ASPECT_RATIO);
    // Inset the top
    result.origin.y += (frame.size.height - result.size.height) / 2;
    // Inset the left
    result.origin.x += IMAGE_INSET;
	
    return result;
}

- (NSRect)_indicatorFrameForInteriorFrame:(NSRect)frame {
    NSRect imageFrame = [self _imageFrameForInteriorFrame:frame];
    NSRect result = frame;
    // Move our inset to the left of the image frame
    result.origin.x = NSMaxX(imageFrame) + INSET_FROM_IMAGE_TO_TEXT;
    // Set the size
	result.size.height = INDICATOR_HEIGHT;
    result.size.width = NSMaxX(frame) - NSMinX(result) - 8*INSET_FROM_IMAGE_TO_TEXT;
    // Move the title above the Y centerline of the image. 
    result.origin.y = floor(NSMidY(imageFrame) - result.size.height/2);
	
    return result;
}

- (NSRect)_cancelButtonFrameForInteriorFrame:(NSRect)frame {
    NSRect indicatorFrame = [self _indicatorFrameForInteriorFrame:frame];
    NSRect result = frame;
    // Move our inset to the left of the indicator frame
    result.origin.x = NSMaxX(indicatorFrame) + INSET_FROM_IMAGE_TO_TEXT;
    // Set the size
	result.size.height = 16;
    result.size.width = 16;
    // Move the title above the Y centerline of the image. 
    result.origin.y = floor(NSMidY(indicatorFrame) - result.size.height/2);
	
    return result;
}

- (NSRect)_titleFrameForInteriorFrame:(NSRect)frame {
    NSRect indicatorFrame = [self _indicatorFrameForInteriorFrame:frame];
    NSRect result = frame;
    // Move our inset to the left of the indicator frame
    result.origin.x = NSMinX(indicatorFrame);
    // Go as wide as we can
    result.size.width = NSMaxX(frame) - NSMinX(result) - INSET_FROM_IMAGE_TO_TEXT;
    result.size.height = TITLE_FONT_SIZE+5;
    // Move the title above the Y centerline of the image. 
	result.origin.y = floor(NSMinY(indicatorFrame) - result.size.height - INSET_FROM_IMAGE_TO_TEXT);
	
	//[[NSColor redColor] set];
	//NSFrameRect(result);
	
    return result;
}

- (NSRect)_infoFrameForInteriorFrame:(NSRect)frame {
    NSRect indicatorFrame = [self _indicatorFrameForInteriorFrame:frame];
    NSRect result = frame;
    // Move our inset to the left of the image frame
    result.origin.x = NSMinX(indicatorFrame);
    // Go as wide as we can
    result.size.width = NSMaxX(frame) - NSMinX(result) - INSET_FROM_IMAGE_TO_TEXT;
    result.size.height = INFO_FONT_SIZE+2;
    // Move the title above the Y centerline of the image. 
    result.origin.y = floor(NSMaxY(indicatorFrame) + INSET_FROM_IMAGE_TO_TEXT);
	
	//[[NSColor redColor] set];
	//NSFrameRect(result);
	
    return result;
}


- (void)setProgressIndicator:(NSProgressIndicator *)indicator {
	[item setValue:indicator forKey:@"ProgressIndicator"];
	//[(NSMutableDictionary *)[self objectValue] setValue:indicator forKey:@"ProgressIndicator"];
}

- (void)setProcess:(WBFSProcess *)process {
	[item setValue:process forKey:@"Process"];
	//[(NSMutableDictionary *)[self objectValue] setValue:process forKey:@"Process"];
}

- (void)ensureProgressIndicatorCreated {
	NSProgressIndicator *indicator = [self progressIndicator];
	if (indicator == nil) {
		indicator = [self initProcessIndicator];
		[indicator startAnimation:nil];
		[self setProgressIndicator:indicator];
	}
}

- (void)drawIndicatorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView {
	[self ensureProgressIndicatorCreated];
	NSProgressIndicator *indicator = [self progressIndicator];

	if ([indicator superview] == nil) [controlView addSubview:indicator];
	[indicator setFrame:cellFrame];
	WBFSProcess *task = [self process];
	float progress = [task progress];
	if ([task isRunning]) {
		if (progress == 0) {
			[indicator setIndeterminate:YES];
			[indicator startAnimation:nil];
			[indicator setDoubleValue:0];
		} else if (progress == 100.0) {
			[indicator setIndeterminate:YES];
			[indicator startAnimation:nil];
			[indicator setDoubleValue:100];
		} else {
			[indicator setIndeterminate:NO];
			[indicator setDoubleValue:progress];
		}
	} else {
		if ([task isTerminated]) 
			progress = 100;
		[indicator setIndeterminate:NO];
		[indicator setDoubleValue:progress];
	}
}

- (void)drawInteriorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView {
	if (_imageCell) {
		NSRect imageFrame = [self _imageFrameForInteriorFrame:cellFrame];
		[_imageCell drawWithFrame:imageFrame inView:controlView];
	}
	
	NSRect indicatorFrame = [self _indicatorFrameForInteriorFrame:cellFrame];
	[self drawIndicatorWithFrame:indicatorFrame inView:controlView];

	if (_infoCell) {
		NSRect infoFrame = [self _infoFrameForInteriorFrame:cellFrame];
		[_infoCell drawInteriorWithFrame:infoFrame inView:controlView];
	}
	
	if (_cancelButtonCell) {
		NSRect buttonFrame = [self _cancelButtonFrameForInteriorFrame:cellFrame];
		[_cancelButtonCell drawInteriorWithFrame:buttonFrame inView:controlView];
	}
	
	NSRect titleFrame = [self _titleFrameForInteriorFrame:cellFrame];
    [super drawInteriorWithFrame:titleFrame inView:controlView];
	
}

@end
