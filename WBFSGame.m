//
//  WBFSGame.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 21/04/10.
//  Copyright 2010 INSERM U556. All rights reserved.
//

#import "WBFSGame.h"
#import "WBFSProcess.h"

#import"WBFSDatabase.h"

@implementation WBFSGame

@synthesize gameId = _gameId, name = _name, sizeGB = _sizeGB, format = _format, path = _path, region = _region, releaseDate = _releaseDate, genre = _genre;

- (WBFSGame *)initWithFile:(NSString *)file {
	WBFSProcess *lsProcess = [[WBFSProcess alloc] init];
	[lsProcess setSource:file];
	if ([[file pathExtension] isEqualToString:@"iso"]) {
		[lsProcess setCommand:kWBFSProcessCommandISOInfo];
	} else {
		[lsProcess setCommand:kWBFSProcessCommandMakeInfo];
	}
	NSString *log = [lsProcess launchAndWaitUntilTermination];
	//[lsProcess release];
	
	// If an error occured it is not a game and return nil
	NSRange errRange = [log rangeOfString:@"ERROR:"];
	//BOOL error = !NSLocationInRange(NSNotFound, errRange);
	BOOL error = (errRange.location!=NSNotFound);
	if (error) {
		return nil;
	}
	self = [self initWithString:log];
	self.path = file;
	self.format = [[file pathExtension] uppercaseString];
	if ([self.name isEqualToString:kGameTitleUnknown]) {
		self.name = [file lastPathComponent];
	}
	return self;
}

- (WBFSGame *)initWithString:(NSString *)str {
	if (self = [super init]) {
		[self parseString: str];
	
		// Read info of the game in Database
		self.region = [[WBFSDatabase loadDatabase] stringValueForXPath:@"region" ofGameID:self.gameId];
		NSString *locale = @"EN";
		if ([[self region] isEqualToString:@"PAL"]) {
			locale = @"FR";
		}
		NSString *title = [[WBFSDatabase loadDatabase] stringValueForXPath:[NSString stringWithFormat:@"locale.lang=%@/title", locale] ofGameID:self.gameId];
		if ([title isNotEqualTo:@""]) self.name = title;
		self.genre = [[WBFSDatabase loadDatabase] stringValueForXPath:@"genre" ofGameID:self.gameId];
		NSString *year = [[WBFSDatabase loadDatabase] stringValueForXPath:@"date.year" ofGameID:self.gameId];
		NSString *month = [[WBFSDatabase loadDatabase] stringValueForXPath:@"date.month" ofGameID:self.gameId];
		NSString *day = [[WBFSDatabase loadDatabase] stringValueForXPath:@"date.day" ofGameID:self.gameId];
		NSString *dateString = [NSString stringWithFormat:@"%@-%@-%@ 00:00:00 +0100", year, month, day];
		self.releaseDate = [NSDate dateWithString:dateString];
		//
	}
	
	return self;
}

- (void)parseString:(NSString *)str {
	NSArray *list = [str componentsSeparatedByString:@"\n"];
	if ([list count]<=8) { // The output contains less than 5 lines WBFS GAME
		NSUInteger i = 0;
		NSRange readRange;
		// Check if it process another file
		// if YES read the next line 
		readRange = [[list objectAtIndex:i] rangeOfString:@"Read Split:"];
		BOOL readPlus = (readRange.location!=NSNotFound);
		if (readPlus) 
			i++;
		
		// Check if the output is all read
		NSString *line = [list objectAtIndex:i];
		NSUInteger len = [line length];
		if (len>0 && [[line substringWithRange:NSMakeRange(len-1, 1)] isEqualToString:@"G"]) {
			NSScanner *scan = [NSScanner scannerWithString:line];
			
			// Read ID Game
			NSString *ref;
			[scan scanUpToString:@" :" intoString:&ref];
			NSLog(@"ID Game: %@", ref);
			
			//Read the title of the game
			NSUInteger nameLoc = [scan scanLocation]+2;
			NSUInteger sizeLoc = len-5;
			NSString *name = [[line substringWithRange:NSMakeRange(nameLoc, sizeLoc-nameLoc)] 
							  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
			NSLog(@"Title: %@", name);
			
			// Read the size of the game
			float size;
			[scan setScanLocation:sizeLoc];
			[scan scanFloat:&size];
			NSLog(@"Size: %f", size);
			
			// Set the object
			self.gameId = ref;
			self.name = name;
			self.sizeGB = [NSNumber numberWithFloat:size];
			self.format = @"WBFS";
		} else {
			// The ouput is not complete, set an undefined object
			self.gameId = @"XXXXXX";
			self.name = kGameTitleUnknown;
			self.sizeGB = [NSNumber numberWithFloat:0.0];
			self.format = @"WBFS";
		}
	} else if ([list count]>8) { // The output contains at least 9 lines ISO GAME
		// Check if the output is all read
		NSString *line = [list objectAtIndex:[list count]-1];
		NSRange sizeRange = [line rangeOfString:@"scrub gb:"];
		if (!NSLocationInRange(NSNotFound, sizeRange)) {
			NSUInteger i = 1;
			
			// Check if the process contains a WARNING
			// If YES read the next line
			NSRange warnRange = [[list objectAtIndex:i] rangeOfString:@"WARNING"];
			BOOL warning = (warnRange.location!=NSNotFound);
			if (warning) i++;
			
			NSString *line = [list objectAtIndex:i];
			
			// Read ID Game
			NSScanner *scan = [NSScanner scannerWithString:line];
			[scan scanString:@"id:         " intoString:NULL];
			NSString *ref = [line substringWithRange:NSMakeRange([scan scanLocation], 6)];
			NSLog(@"ID Game: %@", ref);
			
			// Read tilte of the game
			line = [list objectAtIndex:i+1];
			scan = [NSScanner scannerWithString:line];
			[scan scanString:@"title:      '" intoString:NULL];
			NSString *name = [line substringWithRange:NSMakeRange([scan scanLocation], [line length]-1-[scan scanLocation])];
			NSLog(@"Title: %@", name);
			
			// Read the size of the game
			float size;	
			line = [list objectAtIndex:i+8];
			scan = [NSScanner scannerWithString:line];
			[scan scanString:@"scrub gb:   " intoString:NULL];
			[scan scanFloat:&size];
			NSLog(@"Size: %f", size);
			
			//Set the object
			self.gameId = ref;
			self.name = name;
			self.sizeGB = [NSNumber numberWithFloat:size];
			self.format = @"ISO";
		} else {
			// The ouput is not complete, set an undefined object
			self.gameId = @"XXXXXX";
			self.name = kGameTitleUnknown;
			self.sizeGB = [NSNumber numberWithFloat:0.0];
			self.format = @"ISO";
		}
	} else {
		// The ouput is not complete, set an undefined object
		self.gameId = @"XXXXXX";
		self.name = kGameTitleUnknown;
		self.sizeGB = [NSNumber numberWithFloat:0.0];
	}
}

- (void)release {
	[_gameId release];
	[_name release];
	[_sizeGB release];
	[_format release];
	[_path release];
	[_region release];
	[_releaseDate release];
	[_genre release];
	
	[super release];
}

@end
