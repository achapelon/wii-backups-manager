//
//  WBFSProcess.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 28/01/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSProcess.h"
#import "WBFSManagerAppDelegate.h"

NSString * const WBFSProcessOptionsRefMapStings[] = { @"-s",
	@"-0",
	@"-2",
	@"-4",
	@"-u",
	@"-z",
	@"-a",
	@"-g",
	@"-1",
	@"-f",
	@"-t",
	@"-x",
	@"-l",
	@"-b",
	@"-d" };
NSString * const WBFSProcessCommandsRefMapStings[] = { @"convert",
	@"scrub",
	@"create",
	@"ls",
	@"df",
	@"make_info",
	@"id_title",
	@"init",
	@"add_iso",
	@"add_wbfs",
	@"rm",
	@"extract_iso",
	@"extract_wbfs",
	@"extract_wbfs_all",
	@"wbfs_copy",
	@"ls_file",
	@"extract_file",
	@"debug_info",
	@"iso_info" };
NSString * const WBFSProcessCommandsDescMapStings[] = { @"Convert"
	@"Scrub"
	@"Create"
	@"List"
	@"Device Information"
	@"Make Information"
	@"ID Title"
	@"Init"
	@"Add ISO"
	@"Add WBFS"
	@"Remove"
	@"Extract ISO"
	@"Extract WBFS"
	@"Extract WBFS All"
	@"WBFFS Copy"
	@"List File"
	@"Extract File"
	@"Debug Info"
	@"ISO Info" };

#define kWBFSProcessNbrCommands 19

@implementation WBFSProcess

@synthesize task;
@synthesize output;
@synthesize source;
@synthesize gameID;
@synthesize destination;
@synthesize commandRef;
@synthesize command;
@synthesize options;

@synthesize log;
@synthesize progress;
@synthesize currentSize;
@synthesize fullSize;
@synthesize rate;
@synthesize elapsedTime;
@synthesize remainingTime;
@synthesize startDate;
@synthesize endDate;

- (id)init {
	return [self initWithBinary: @"wbfs_file"];
}

- (id)initWithBinary:(NSString *)binary {
	if (self = [super init]) {
		// Set up the task
		task = [[NSTask alloc] init];
		[task setLaunchPath:[[NSBundle mainBundle] pathForResource: binary ofType: nil]];
		begun = NO;	
		finished = NO;	
		
		// Initialize output
		log = [[NSMutableString string] retain];
		source = [[NSString string] retain];
		gameID = [[NSString string] retain];
		destination = [[NSString string] retain];
		command = [[NSString string] retain];
		commandRef = 0;
		//options = [NSString string];
		options = [[NSMutableDictionary dictionaryWithCapacity:0] retain];
		progress = 0.0f;
		currentSize = 0.0f;
		fullSize = 0.0f;
		rate = 0.0f;
		elapsedTime = 0;
		remainingTime = NSUndefinedDateComponent;
	}
	
	return self;
}

-(void)release {
	[task release];
	[output release];
	
	[source release];
	[gameID release];
	[destination release];
	[command release];
	[options release];
	
	[log release];
	[description release];
	
	[startDate release];
	[endDate release];
	
	[super release];
}

- (id)copyWithZone:(NSZone *)zone {	
	WBFSProcess *copy =  [[self class] allocWithZone:zone];
	[copy setTask: task];
	[copy setCommand: command];
	[copy setCommandRef: commandRef];
	[copy setOptions: [options retain]];
	[copy setSource: source];
	[copy setDestination: destination];
	[copy setGameID: gameID];
	
	[copy setLog: log];
	[copy setProgress: progress];
	[copy setCurrentSize: currentSize];
	[copy setFullSize: fullSize];
	[copy setRate: rate];
	[copy setRemainingTime: remainingTime];
	[copy setElapsedTime: elapsedTime];
	[copy setStartDate: [startDate retain]];
	[copy setEndDate: [endDate retain]];
		
	return copy;
}

- (void)taskDidRefresh:(NSNotification *)notification {
	WBFSManagerAppDelegate *app = (WBFSManagerAppDelegate *)[[NSApplication sharedApplication] delegate];
	//NSMutableString *logString = [NSMutableString string];
	NSString *bin = [self binary];
	
	if( [notification object] != output )
		return;
	
	NSData *data = [[notification userInfo] objectForKey:NSFileHandleNotificationDataItem];
	NSString *theLog = [[NSString alloc] initWithData:data 
											 encoding:NSASCIIStringEncoding];
	
	if (!begun) {
		//[logString appendFormat:@"%@ %@\n", [[task launchPath] lastPathComponent], [[task arguments] description]];
		
		begun = YES;
	}

	//[logString appendString: theLog];
	
	NSLog(@"%@\n", theLog);
	[log appendString: theLog];
	
	
	if ([bin isEqualToString:@"wbfs_file"]) {
		sscanf([theLog UTF8String], "%f%% (%*c) ETA: %*s (%fMB of %fMB ~ %fMB/s) time: %*fs", &progress, &currentSize, &fullSize, &rate);
	} else {
		sscanf([theLog UTF8String], "%f%% copied in %*s (%fMiB/sec) -> ETA %*s", &progress, &rate );
		currentSize = fullSize * progress/100.0;
	}
	
	[theLog release];
	
	if (progress > 0) {
		elapsedTime = [[NSDate date] timeIntervalSinceDate: startDate];
		remainingTime = (elapsedTime/progress) * (100-progress);
	}
		
	
	if ([task isRunning]) {
		[output readInBackgroundAndNotify];
	} else {
		endDate = [NSDate date];
		elapsedTime = [endDate timeIntervalSinceDate: startDate];
		finished = YES;
		[[NSNotificationCenter defaultCenter] removeObserver:self];
		
		//[logString appendString: @"-----\n"];
	}

	//[app.logView setString: logString];
	
	[app refreshQueue];
}

- (NSArray *)optionsList {
	NSMutableArray *optArr = [NSMutableArray array];
	NSArray *keys = [options allKeys];
	for (NSUInteger i=0; i<[keys count]; i++) {
		NSString *key = [keys objectAtIndex:i];
		NSString *value=[options valueForKey:key];
		[optArr addObject:key];
		if (![value isEqualToString:@"#"]) {
			[optArr addObject:value];
		}
	}
	
	return optArr;
}

- (NSString *)commandStringByRef:(WBFSProcessCommandsRef)ref {
	return WBFSProcessCommandsRefMapStings[ref];
}

- (NSUInteger)commandRefByString:(NSString *)cmd {
	for (int i = 0; i < kWBFSProcessNbrCommands; i++) {
		if ([WBFSProcessCommandsRefMapStings[i] isEqualToString:cmd])
			return i;
	}
	return -1;
}

- (void)setDestinationFilenameLayoutOption:(NSString *)layout {
	[self setValue:layout forOption:kWBFSProcessOptionDestinationFilenameLayout];
}

- (void)setOption:(NSString *)option {
	[self setValue:@"#" forOption:option];
}

- (void)setCommand:(NSString *)aCommand {
	[command release];
	command = [NSString stringWithString:aCommand];
	commandRef = [self commandRefByString:command];
}

- (void)setValue:(NSString *)value forOption:(NSString *)option {
	[options setValue:value forKey:option];
}

- (NSString *)binary {
	return [[task launchPath] lastPathComponent];
}

- (void)updateArguments {
	NSString *bin = [self binary];
	NSMutableArray *args = [NSMutableArray array];
	
	if ([bin isEqualToString:@"wbfs_file"]) {
		[args addObjectsFromArray:[self optionsList]];
		[args addObject:source];
		[args addObject:command];
		if (![gameID isEqualToString:@""])
			[args addObject:gameID];
		[args addObject:destination];
	} else {
		[args addObject:command];
		[args addObject:source];
		[args addObjectsFromArray:[self optionsList]];
	}
	
	[task setArguments:args];
}

- (void)launch {	

	// To catch log of the task
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(taskDidRefresh:) 
												 name:NSFileHandleReadCompletionNotification 
											   object:nil];
	
	
	[self updateArguments];

	NSLog(@"Executing %@ %@...", [task launchPath], [task arguments]);
	// Set up communication channel
	NSPipe *pipe = [NSPipe pipe];
	[task setStandardOutput:pipe];
	output = [pipe fileHandleForReading];	
	[output readInBackgroundAndNotify];
	
	// Run the task
    [task launch];
	
	startDate = [[NSDate date] retain];
}

- (NSString *)launchAndWaitUntilTermination {
	[self launch];
	
	[task waitUntilExit];
	
	// Ensure to return all data read
	NSData *data = [[[task standardOutput] fileHandleForReading] readDataToEndOfFile];
	NSString *theLog = [[NSString alloc] initWithData:data 
										  encoding:NSASCIIStringEncoding];
	[log appendString: theLog];
	
    [theLog release];
	return log;
}

- (BOOL)isTerminated {
	return finished;
}

- (NSString *)description {
	NSString *desc;
	if ([command isEqualToString:@"convert"]) {
		desc = [NSString stringWithFormat:NSLocalizedString(@"CONVERT_COMMAND_DESCRIPTION_FORMAT", @""), [self isRunning] ? @"ing" : @"", [source lastPathComponent], [destination lastPathComponent]];
	} else if ([command isEqualToString:@"add_wbfs"] || 
			   [command isEqualToString:@"add_iso"] || 
			   [command isEqualToString:@"add"]) {
		desc = [NSString stringWithFormat:NSLocalizedString(@"ADD_COMMAND_DESCRIPTION_FORMAT", @""), [self isRunning] ? @"ing" : @"", [destination lastPathComponent], [source lastPathComponent]];
	} else if ([command isEqualToString:@"extract_wbfs"]) {
		desc = [NSString stringWithFormat:NSLocalizedString(@"EXTRACT_COMMAND_DESCRIPTION_FORMAT", @""), [self isRunning] ? @"ing" : @"", gameID, [destination lastPathComponent]];
	} else {
		desc = [NSString stringWithString:NSLocalizedString(@"UNKNOWN_COMMAND_DESCRIPTION", @"")];
	}
	
	return desc;
}

- (BOOL)isRunning {
	//return ([task isRunning] && ![self isTerminated]);
	return [task isRunning];
}

@end
