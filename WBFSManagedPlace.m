//
//  WBFSManagedPlace.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 27/03/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSManagedPlace.h"
#import "WBFSManagedObjectContext.h"

@implementation WBFSManagedPlace

@synthesize icon;

+ (WBFSManagedPlace *)insertWithPath:(NSString *)path {
	//return [[[[self class] alloc] autorelease] insertWithPath:path];
	return [[[self class] alloc] insertWithPath:path];
}

- (WBFSManagedPlace *)initWithPath:(NSString *)path {
	if ( [super init]) {
		self.path = path;
	}
	return self;
}

- (WBFSManagedPlace *)insertWithPath:(NSString *)path {
	WBFSManagedObjectContext *context = [WBFSManagedObjectContext context];
	NSEntityDescription *entity = [NSEntityDescription entityForName:kPlaceEntityName inManagedObjectContext:context];
	if ((self = [super initWithEntity:entity insertIntoManagedObjectContext:context])) {
		self.path = path;
		self.icon = [[NSWorkspace sharedWorkspace] iconForFile:path];
	}
	
	return self;
}

- (NSString *)path {
	return [self primitiveValueForKey:kPlacePathKey];
}

- (NSString *)name {
	return [[self path] lastPathComponent];
}

- (void)setPath:(NSString *)path {
	[self setPrimitiveValue:path forKey:kPlacePathKey];
}

@end	
