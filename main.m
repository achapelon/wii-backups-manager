//
//  main.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 26/01/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
