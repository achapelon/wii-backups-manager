//
//  WBFSGamesTableView.m
//  Wii Backups Manager
//
//  Created by Anthony on 27/06/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WBFSGamesTableView.h"
#import "WBFSManagedGame.h"

@implementation WBFSGamesTableView

- (void)keyDown:(NSEvent *)event
{	
	[super keyDown:event];
	
	switch ( [[event charactersIgnoringModifiers] characterAtIndex:0] ) {
		case ' ':
			[[NSApp delegate] performSelector:@selector(togglePreviewPanel:)];
			break;
		default:
			break;
	}
}
/*
- (void)mouseDown:(NSEvent *)theEvent
{
    NSPoint dragPosition;
    NSRect imageLocation;
	
    dragPosition = [self convertPoint:[theEvent locationInWindow]
							 fromView:nil];
    dragPosition.x -= 16;
    dragPosition.y -= 16;
    imageLocation.origin = dragPosition;
    imageLocation.size = NSMakeSize(32,32);
    [self dragPromisedFilesOfTypes:[NSArray arrayWithObject:WBFSManagedGameDataType]
						  fromRect:imageLocation
							source:self
						 slideBack:YES
							 event:theEvent];
}
*/

- (NSArray *)namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination {
	return [NSArray array];
}

@end
