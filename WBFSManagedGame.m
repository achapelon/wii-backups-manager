//
//  WBFSManagedGame.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 25/03/10.
//  Copyright 2010. All rights reserved.
//

#import <ImageKit/ImageKit.h>

#import "WBFSManagedGame.h"
#import "WBFSGame.h"
#import "WBFSProcess.h"
#import "WBFSManagedObjectContext.h"
#import "WBFSManagerAppDelegate.h"

@implementation WBFSManagedGame

@synthesize image = _imageCache;

+ (WBFSManagedGame *)insertGame:(WBFSGame *)game {
	return [[[[self class] alloc] autorelease] insertGame:game];
	//return [[[self class] alloc] insertGame:game];
}

- (WBFSManagedGame *)insertGame:(WBFSGame *)game {
	NSManagedObjectContext *context = [WBFSManagedObjectContext context];
	NSEntityDescription *entity = [NSEntityDescription entityForName:kGameEntityName inManagedObjectContext:context];
	if ((self = [super initWithEntity:entity insertIntoManagedObjectContext:context])) {
		self.gameId = game.gameId;
		self.name = game.name;
		self.sizeGB = game.sizeGB;
		self.format = game.format;
		self.path = game.path;
		self.region = game.region;
		self.releaseDate = game.releaseDate;
		self.genre = game.genre;
		_imageUID = [[NSString stringWithFormat:@"-%d", rand()] retain];
	}
	
	return self;
}

- (NSString *)gameId {
	return [self primitiveValueForKey:kGameIDKey];
}

- (NSString *)name {
	return [self primitiveValueForKey:kGameNameKey];
}

- (NSString *)path {
	return [self primitiveValueForKey:kGameFilePathKey];
}

- (NSString *)region {
	return [self primitiveValueForKey:kGameRegionKey];
}

- (NSNumber *)sizeGB {
	return [self primitiveValueForKey:kGameSizeGBKey];
}

- (NSString *)format {
	return [self primitiveValueForKey:kGameFormatKey];
}

- (NSNumber *)rating {
	return [self primitiveValueForKey:kGameRatingKey];
}

- (NSDate *)releaseDate {
	return [self primitiveValueForKey:kGameReleaseDateKey];
}

- (NSString *)genre {
	return [self primitiveValueForKey:kGameGenreKey];;
}

- (void)setGameId:(NSString *)Id {
	[self setPrimitiveValue:Id forKey:kGameIDKey];
}

- (void)setName:(NSString *)name {
	[self setPrimitiveValue:name forKey:kGameNameKey];
}

- (void)setPath:(NSString *)path {
	[self setPrimitiveValue:path forKey:kGameFilePathKey];
}

- (void)setRegion:(NSString *)region {
	[self setPrimitiveValue:region forKey:kGameRegionKey];
}

- (void)setSizeGB:(NSNumber *)sizeGB {
	[self setPrimitiveValue:sizeGB forKey:kGameSizeGBKey];
}

- (void)setFormat:(NSString *)format {
	[self setPrimitiveValue:format forKey:kGameFormatKey];
}

- (void)setRating:(NSNumber *)rating {
	[self setPrimitiveValue:rating forKey:kGameRatingKey];
}

- (void)setReleaseDate:(NSDate *)date {
	[self setPrimitiveValue:date forKey:kGameReleaseDateKey];
}

- (void)setGenre:(NSString *)genre {
	[self setPrimitiveValue:genre forKey:kGameGenreKey];
}

- (void)setImage:(NSImage *)image {
	[self clearCachedImage];
	_imageCache = [image copy];
	
	// Save the file
	NSString *folderPath = [[NSApp delegate] applicationSupportDirectory];
	folderPath = [folderPath stringByAppendingPathComponent:@"Artworks"];
	folderPath = [folderPath stringByAppendingPathComponent:@"Covers3D"];
	folderPath = [folderPath stringByAppendingPathComponent:@"FR"];
	NSString *imagePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [self gameId]]];
	[[image TIFFRepresentation] writeToFile:imagePath atomically:YES]; 
}

- (NSString *)localeForArtworks {
	NSString *locale = @"US";
	if ([[self region] isEqualToString:@"PAL"]) {
		locale = @"FR";
	}
	return locale;
}

- (NSImage *)imageCover3D {
	return [self imageCover3DForLocale:[self localeForArtworks]];
}

- (NSImage *)imageCover3DForLocale:(NSString *)locale {
	locale = [locale uppercaseString];
	NSFileManager *manager = [NSFileManager defaultManager];
	NSString *folderPath = [[NSApp delegate] applicationSupportDirectory];
	folderPath = [folderPath stringByAppendingPathComponent:@"Artworks"];
	folderPath = [folderPath stringByAppendingPathComponent:@"Covers3D"];
	folderPath = [folderPath stringByAppendingPathComponent:locale];
	NSString *imagePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [self gameId]]];
	
	if (![manager fileExistsAtPath:imagePath]) {
		NSURL *url = [NSURL URLWithString:
				  [NSString stringWithFormat:kImageURLCover3DFormat, locale, [self gameId]]];
		
		// Cache the image with the one downloaded at the url
		[manager createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
		[[NSData dataWithContentsOfURL:url] writeToFile:imagePath atomically:YES];
	}
	
	//return [[[NSImage alloc] initByReferencingFile:imagePath] autorelease];
	return [[NSImage alloc] initByReferencingFile:imagePath];
}

- (NSImage *)imageCover { 
	return [self imageCoverForLocale:[self localeForArtworks]];
}
	
- (NSImage *)imageCoverForLocale:(NSString *)locale {
	locale = [locale uppercaseString];
	NSFileManager *manager = [NSFileManager defaultManager];
	NSString *path = [[NSApp delegate] applicationSupportDirectory];
	path = [path stringByAppendingPathComponent:@"Artworks"];
	path = [path stringByAppendingPathComponent:@"Covers"];
	path = [path stringByAppendingPathComponent:locale];
	[manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [self gameId]]];
	
	NSImage *image;
	if ([manager fileExistsAtPath:path]) {
		image = [[NSImage alloc] initByReferencingFile:path];
	} else {
		NSURL *url = [NSURL URLWithString:
					  [NSString stringWithFormat:kImageURLCoverFormat, locale, [self gameId]]];
		// Cache the image
		[[NSData dataWithContentsOfURL:url] writeToFile:path atomically:YES];
		image = [[NSImage alloc] initByReferencingURL:url];
	}
	
	//return [image autorelease];
	return image;
}

- (NSImage *)imageCoverfull {
	return [self imageCoverfullForLocale:[self localeForArtworks]];
}

- (NSImage *)imageCoverfullForLocale:(NSString *)locale {
	locale = [locale uppercaseString];
	NSFileManager *manager = [NSFileManager defaultManager];
	NSString *path = [[NSApp delegate] applicationSupportDirectory];
	path = [path stringByAppendingPathComponent:@"Artworks"];
	path = [path stringByAppendingPathComponent:@"Coversfull"];
	path = [path stringByAppendingPathComponent:locale];
	[manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [self gameId]]];
	
	NSImage *image;
	if ([manager fileExistsAtPath:path]) {
		image = [[NSImage alloc] initByReferencingFile:path];
	} else {
		NSURL *url = [NSURL URLWithString:
					  [NSString stringWithFormat:kImageURLCoverfullFormat, locale, [self gameId]]];
		// Cache the image
		[[NSData dataWithContentsOfURL:url] writeToFile:path atomically:YES];
		image = [[NSImage alloc] initByReferencingURL:url];
	}
	
	//return [image autorelease];
	return image;
}

- (NSImage *)imageCoverfullHQ { 
	return [self imageCoverfullHQForLocale:[self localeForArtworks]];
}

- (NSImage *)imageCoverfullHQForLocale:(NSString *)locale {
	locale = [locale uppercaseString];
	NSFileManager *manager = [NSFileManager defaultManager];
	NSString *path = [[NSApp delegate] applicationSupportDirectory];
	path = [path stringByAppendingPathComponent:@"Artworks"];
	path = [path stringByAppendingPathComponent:@"CoversfullHQ"];
	path = [path stringByAppendingPathComponent:locale];
	[manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [self gameId]]];
	
	NSImage *image;
	if ([manager fileExistsAtPath:path]) {
		image = [[NSImage alloc] initByReferencingFile:path];
	} else {
		NSURL *url = [NSURL URLWithString:
					  [NSString stringWithFormat:kImageURLCoverfullHQFormat, locale, [self gameId]]];
		// Cache the image
		[[NSData dataWithContentsOfURL:url] writeToFile:path atomically:YES];
		image = [[NSImage alloc] initByReferencingURL:url];
	}
	
	//return [image autorelease];
	return image;
}

- (NSImage *)imageDisc { 
	return [self imageDiscForLocale:[self localeForArtworks]];
}

- (NSImage *)imageDiscForLocale:(NSString *)locale {
	locale = [locale uppercaseString];
	NSFileManager *manager = [NSFileManager defaultManager];
	NSString *path = [[NSApp delegate] applicationSupportDirectory];
	path = [path stringByAppendingPathComponent:@"Artworks"];
	path = [path stringByAppendingPathComponent:@"Discs"];
	path = [path stringByAppendingPathComponent:locale];
	[manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", [self gameId]]];
	
	NSImage *image;
	if ([manager fileExistsAtPath:path]) {
		image = [[NSImage alloc] initByReferencingFile:path];
	} else {
		NSURL *url = [NSURL URLWithString:
					  [NSString stringWithFormat:kImageURLDiscFormat, locale, [self gameId]]];
		// Cache the image
		[[NSData dataWithContentsOfURL:url] writeToFile:path atomically:YES];
		image = [[NSImage alloc] initByReferencingURL:url];
	}
	
	//return [image autorelease];
	return image;
}

- (NSString *)description {
	return [NSString stringWithFormat:@"%@ : %@ %.2@G", self.gameId, self.name, self.sizeGB];
}

- (void)clearCachedImage {
	[_imageUID release];
	_imageUID = [[NSString stringWithFormat:@"-%d", rand()] retain];
	[_imageCache release];
	_imageCache = nil;
}

#pragma mark -
#pragma mark Methods for IKImageFlowView Protocol

- (NSString *)imageUID
{
	return _imageUID;
}

- (NSString *)imageRepresentationType
{
	return IKImageBrowserNSImageRepresentationType;
}

- (id)imageRepresentation
{
	if ( _imageCache==nil ) {
		WBFSManagerAppDelegate *app = [NSApp delegate];
		switch (app.coverFlowType) {
			case 0:
				_imageCache = [self imageCover3D];
				break;
			case 1:
				_imageCache = [self imageCover];
				break;
			case 2:
				_imageCache = [self imageDisc];
				break;
		}
		
		/*if ( ![_imageCache isValid] ) {
			[_imageCache release];
			_imageCache == [NSImage imageNamed:@"cover3DNoImage.png"];
		}*/
	}
	return _imageCache;
}

- (NSString *)imageTitle
{
	return self.name;
}

#pragma mark -
#pragma mark Methods for QLPreviewItem Protocol

- (NSURL *)previewItemURL {
	return [NSURL URLWithString:self.path]; 
}

@end
