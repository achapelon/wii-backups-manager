//
//  WBFSManagedPlaylist.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 04/08/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSManagedPlaylist.h"
#import "WBFSManagedObjectContext.h"

@implementation WBFSManagedPlaylist

@synthesize icon;

+ (WBFSManagedPlaylist *)insertWithName:(NSString *)name {
	//return [[[[self class] alloc] initWithName:name] autorelease];
	return [[[self class] alloc] initWithName:name];
}

- (WBFSManagedPlaylist *)initWithName:(NSString *)name {
	WBFSManagedObjectContext *context = [WBFSManagedObjectContext context];
	NSEntityDescription *entity = [NSEntityDescription entityForName:kPlaylistEntityName inManagedObjectContext:context];
	if ((self = [super initWithEntity:entity insertIntoManagedObjectContext:context])) {
		self.name = name;
	}
	return self;
}

- (NSString *)name {
	return [self primitiveValueForKey:kPlaylistNameKey];
}

- (void)setName:(NSString *)name {
	[self setPrimitiveValue:name forKey:kPlaylistNameKey];
}

- (NSString *)predicate {
	return [self primitiveValueForKey:kPlaylistPredicateKey];
}

- (void)setPredicate:(NSString *)predicate {
	[self setPrimitiveValue:predicate forKey:kPlaylistPredicateKey];
}

- (NSSet *)games {
	return [self primitiveValueForKey:kPlaylistGamesKey];	
}

- (void)setGames:(NSSet *)set {
	[self setPrimitiveValue:set forKey:kPlaylistGamesKey];
}

- (void)release
{
	[icon release];
	[super release];
}

@end	
