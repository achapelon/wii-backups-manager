//
//  WBFSQueueProcessController.m
//
//  Created by Anthony Chapelon on 29/01/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSActivityController.h"
#import "WBFSProcessCell.h"
#import "WBFSProcess.h"

@implementation WBFSActivityController

//@synthesize window;
@synthesize processesTableView;
@synthesize processesList;

- (void)setup {
	processesList = [[NSMutableArray array] retain];
	//[self windowView];
	[processesTableView setDataSource:self];
	[processesTableView setDelegate:self];
	[[processesTableView tableColumnWithIdentifier:@"ActivityColumn"] setDataCell:[[[WBFSProcessCell alloc] init] autorelease]];
		
	[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(reloadProcessesActivity) userInfo:nil repeats:YES];
}

- (void)addProcessActivity:(WBFSProcess *)aProcess {
	NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObject:aProcess forKey:@"Process"];
	[processesList addObject:dict];
	//[processesTableView reloadData];
	[self reloadProcessesActivity];
}

- (void)refresh {
	[processesTableView reloadData];
}

- (IBAction)removeSelectedProcessesActivity:(id)sender {
	NSIndexSet *indexes = [processesTableView selectedRowIndexes];
	[indexes enumerateIndexesWithOptions:NSEnumerationReverse usingBlock:^(NSUInteger idx, BOOL *stop) {
		id obj = [processesList objectAtIndex:idx];
		WBFSProcess *process = [obj valueForKey:@"Process"];
		if (![process isRunning]) {
			[self removeProcessActivity: obj];
		}
	}];
	[processesTableView reloadData];
}


- (IBAction)removeAllFinishedProcessesActivity:(id)sender {
	[processesList enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		WBFSProcess *process = [obj valueForKey:@"Process"];
		if ([process isTerminated]) {
			[self removeProcessActivity: obj];
		}
	}];
	[processesTableView reloadData];
}
/*
- (IBAction)removeAllFinishedProcessesActivity:(id)sender {
	for (NSUInteger i = [processesList count]; i > 0; i--) {
		NSDictionary *process = [processesList objectAtIndex: i-1];
		WBFSProcess *task = [process valueForKey: @"Process"];
		if ([task isTerminated]) {
			[self removeProcessActivity: process];
		}
	}
	[processesTableView reloadData];
}
*/

- (void)removeProcessActivity:(NSDictionary *)process {
	NSProgressIndicator *indicator = [process valueForKey:@"ProgressIndicator"];
	[indicator removeFromSuperview];
	[processesList removeObject: process];
}

- (void)reloadProcessesActivity {
	WBFSProcess *nextProcess = [self nextProcessActivityToLaunch];
	if (nextProcess != nil) {
		[nextProcess launch]; // On lance la prochain process si aucun ne tourne
	}
	
	[processesTableView reloadData];
	
	[self updateDockIcon];
	
	[self updateStatusText];

}

- (WBFSProcess *)nextProcessActivityToLaunch {
	for (NSDictionary *process in processesList) {
		WBFSProcess *task = [process valueForKey:@"Process"];
		if ([task isRunning])
			return nil;
		else if (![task isTerminated])
			return task;
	}
	return nil;
}

- (BOOL)isRunning {
	for (NSDictionary *process in processesList) {
		if ([[process objectForKey:@"Process"] isRunning]) {
			return YES;
		}
	}
	return NO;
}

- (void)updateDockIcon {	
	// get our Dock tile to this app
	NSDockTile *dockTile = [NSApp dockTile];
	// setup our image view for the dock tile		
	NSRect frame = NSMakeRect(0, 0, dockTile.size.width, dockTile.size.height);
	// By default, set the dock tile content view with the icon app
	NSImageView *dockImageView = [[NSImageView alloc] initWithFrame: frame];
	[dockImageView setImage: [NSImage imageNamed:@"iconApp.icns"]];
	[dockTile setContentView:dockImageView];
	
	// Shows a pogress bar if running processes are active
	if (taskFinishing || totalOfRunningProgress > 0) {
		// setup the progress bar
		NSProgressIndicator *progressIndicator = [[NSProgressIndicator alloc] initWithFrame:NSMakeRect(0.0f, 0.0f, dockTile.size.width, 20.0f)];
		[progressIndicator setStyle:NSProgressIndicatorBarStyle];
		[progressIndicator setIndeterminate:taskFinishing];
		[progressIndicator startAnimation:nil];
		if (!taskFinishing) {
			double progress = ((double)(numberOfFinishedTask)+(totalOfRunningProgress/100.0))/(double)(numberOfTask)*100;
			[progressIndicator setMinValue:0];
			[progressIndicator setMaxValue:100];
			[progressIndicator setDoubleValue:progress];
		}
		
		// add it to the image view
		[dockImageView addSubview:progressIndicator];
		[progressIndicator release];
		
		// update the dock tile content view by our image view
		[dockTile setContentView:dockImageView];
	}
	[dockImageView release];
	
	// Shows completed tasks if at least one task is finish
	if (numberOfFinishedTask > 0)
		[dockTile setBadgeLabel: [NSString stringWithFormat:@"%d", numberOfFinishedTask]];
	else
		[dockTile setBadgeLabel: @""];

	// force to display our custom dock tile
	[dockTile display];
	
}

- (void)updateStatusText {
	if (numberOfFinishedTask > 0) {
		[statusText setStringValue:[NSString stringWithFormat:NSLocalizedString(@"NBR_FINISHED_OF_TOTAL_ACTIVITIES_FORMAT", @"") , numberOfFinishedTask, numberOfTask]];
	} else {
		[statusText setStringValue:[NSString stringWithFormat:NSLocalizedString(@"NBR_ACTIVITIES_FORMAT", @"") , numberOfTask]];
	}
}

#pragma mark -
#pragma mark NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView {
	numberOfTask = [processesList count];
	numberOfFinishedTask = 0;
	totalOfRunningProgress = 0;
	taskFinishing = NO;
	
	for (NSDictionary *process in processesList) {
		WBFSProcess *task = [process valueForKey:@"Process"];
		if ([task isTerminated])
			numberOfFinishedTask++;
		else if ([task progress] == 100 ) {
			taskFinishing = YES;
			break;
		} else 
			totalOfRunningProgress += [task progress];
	}
	
	return numberOfTask;
}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex {
	NSMutableDictionary *proc = [processesList objectAtIndex:rowIndex];
	WBFSProcess *task = [proc objectForKey:@"Process"];
	return [task description];
}

#pragma mark -
#pragma mark NSTableViewDelegate

- (void)tableView:(NSTableView *)aTableView willDisplayCell:(id)aCell forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex {
	NSMutableDictionary *proc = [processesList objectAtIndex:rowIndex];
	
	//[aCell prepareCell];
	[aCell setItem:proc];
	
	WBFSProcess *task = [proc objectForKey:@"Process"];
	float progress = task.progress;
	NSString *info = @"";
	if ([task isRunning]) {
		if (progress == 100) {
			info = @"Finishing...";
		} else {
			NSTimeInterval rtime = task.remainingTime;
			NSUInteger min = (int)(rtime/60);
			NSUInteger sec = rtime-min*60;
			/*
			if (min > 0 && sec > 0) {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_MINSEC_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate, min, sec];
			} else if (min > 0 && sec == 0) {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_MIN_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate, min];
			} else if (sec > 0) {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_SEC_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate, sec];
			} else if (rtime == NSUndefinedDateComponent) {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_ESTIMATINGTIME_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate];
			} else {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_NOTIME_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate];
			}
			*/
			if (min > 0) {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_MIN_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate, min];
			} else if (sec > 0) {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_SEC_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate, sec];
			} else if (rtime == NSUndefinedDateComponent) {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_ESTIMATINGTIME_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate];
			} else {
				info = [NSString stringWithFormat:NSLocalizedString(@"PROGRESS_NOTIME_TASK_FORMAT", @""), 
						task.currentSize, task.fullSize, task.rate];
			}
		}
	} else if ([task isTerminated]) {
		NSTimeInterval etime = task.elapsedTime;
		NSUInteger min = (int)(etime/60);
		NSUInteger sec = etime-min*60;
		if (min > 0 && sec > 0) {
			info = [NSString stringWithFormat:NSLocalizedString(@"DONE_MINSEC_TASK_FORMAT", @""), min, sec];
		} else if (min > 0) {
			info = [NSString stringWithFormat:NSLocalizedString(@"DONE_MIN_TASK_FORMAT", @""), min];
		} else {
			info = [NSString stringWithFormat:NSLocalizedString(@"DONE_SEC_TASK_FORMAT", @""), sec];
		}
		//info = [NSString stringWithString:[task status]];
	}
	
	[aCell setInfo:info];
	if ([[task command] isEqualToString:kWBFSProcessCommandExtractFile] ||
		[[task command] isEqualToString:kWBFSProcessCommandExtractISO] ||
		[[task command] isEqualToString:kWBFSProcessCommandExtractWBFS]) {
		[aCell setImage:[NSImage imageNamed:@"extractGame.png"]];
	} else if ([[task command] isEqualToString:kWBFSProcessCommandConvert]) {
		[aCell setImage:[NSImage imageNamed:@"convertGame.png"]];
	} else if ([[task command] isEqualToString:kWBFSProcessCommandAdd] ||
			   [[task command] isEqualToString:kWBFSProcessCommandAddWBFS] ||
			   [[task command] isEqualToString:kWBFSProcessCommandAddWBFS]) {
		[aCell setImage:[NSImage imageNamed:@"addGame.png"]];
	}
}

@end
