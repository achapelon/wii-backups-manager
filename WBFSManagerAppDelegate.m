//
//  WBFS_File_GUIAppDelegate.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 26/01/10.
//  Copyright 2010. All rights reserved.
//

#include "WBFSDiskManagment.h"

#import "WBFSManagerAppDelegate.h"

#import "WBFSActivityController.h"
#import "WBFSProcess.h"

#import "WBFSManagedObjectContext.h"
#import "WBFSManagedGame.h"
#import "WBFSManagedDevice.h"
#import "WBFSManagedPlace.h"
#import "WBFSManagedPlaylist.h"
#import "WBFSGame.h"

#import "ImageAndTextCell.h"
#import "IKImageFlowView.h"

#import "WBFSDatabase.h"

void fsevents_callback(ConstFSEventStreamRef streamRef,
                       void *userData,
                       size_t numEvents,
                       void *eventPaths,
                       const FSEventStreamEventFlags eventFlags[],
                       const FSEventStreamEventId eventIds[]);

@implementation WBFSManagerAppDelegate

@synthesize addPlaylistButton, window, queueController, managedObjectContext, gamesList, coverFlowView, coverFlowType, logView;

- (id)init {
	if ([super init]) {
		predicate = [[self predicateForNewSmartPlaylist] retain];
	}
	return self;
}

- (void)awakeFromNib {
	[self loadPrefs];
	[[WBFSDatabase alloc] downloadLatestDatabase];
	
	[self setupQuickLook];
	[self setupWindowDisplay];
	[self setupSourcesList];
	[self setupGamesList];
	[self setupCoverFlow];
	[self setupCoverView];
	[queueController setup];
}

#pragma mark -
#pragma mark Instance Methods

/**
 Returns the managed object context for the application 
 */
- (WBFSManagedObjectContext *)managedObjectContext {
	if (managedObjectContext) return managedObjectContext;
	
	managedObjectContext = [WBFSManagedObjectContext context];
	return managedObjectContext;
}

- (NSPredicate *)predicateForNewSmartPlaylist {
	return [NSPredicate predicateWithFormat:@"name = ''"];
}

/**
 Returns the support directory for the application, used to store the Core Data
 store file, Artworks, etc.
 */
- (NSString *)applicationSupportDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : NSTemporaryDirectory();
    return [basePath stringByAppendingPathComponent:[[[NSBundle mainBundle] executablePath] lastPathComponent]];
}

- (void)displayView:(NSView *)view {
	// Copie dans une nouvelle vue pour ne pas alterer l'originale
	// Je ne sais pas pourquoi l'origine est modifiée à chaque appel => il faut la réinitialisé manuellement à [0;0]
	[view setFrameOrigin:NSMakePoint(0, 0)];
	NSView *theView = [[NSView alloc] initWithFrame:[view frame]];
	[theView addSubview:view];
	
	NSView *currentView = [window contentView];
	NSRect windowFrame = [window frame];
	NSRect theViewFrame = [theView frame];
	
	// On modifie l'origine en fonction de la hauteur de la nouvelle vue pour que le haut de la fenêtre soit toujours le même
	theViewFrame.origin = windowFrame.origin;
	theViewFrame.origin.y += windowFrame.size.height;
    theViewFrame.origin.y -= theViewFrame.size.height;
	
	// On supprime la vue de la fenêtre pendant le redimensionnement pour que ca soit plus jolie visuellement
	[currentView removeFromSuperview];
	[window setFrame:theViewFrame display:YES animate:YES];
	// On affiche la nouvelle vue dans la fenêtre
	[window setContentView:theView];
    
    [theView release];
}

- (void)openCoverViewWithAnimation:(BOOL)animate {
	NSRect leftPaneRect = [leftPane bounds];
	NSRect sourcesRect = [sourcesView bounds];
	NSRect coverRect = [coverView bounds];
	
	coverRect.origin.y = 0;
	
	sourcesRect.origin.y = NSHeight(coverRect)-1;
	sourcesRect.size.height = NSHeight(leftPaneRect)-NSHeight(coverRect)+1;
	sourcesRect.size.width = NSWidth(leftPaneRect)+1;
	
	if (animate) {
		[[sourcesView animator] setFrame:sourcesRect];
		[[coverView animator] setFrame:coverRect];
	} else {
		[sourcesView setFrame:sourcesRect];
		[coverView setFrame:coverRect];
	}
	
	[toggleCoverViewButton setImage:[NSImage imageNamed:@"Down.png"]];
	
	[coverTableView reloadData];
}

- (void)closeCoverViewWithAnimation:(BOOL)animate {
	NSRect leftPaneRect = [leftPane bounds];
	NSRect sourcesRect = [sourcesView bounds];
	NSRect coverRect = [coverView bounds];

	coverRect.origin.y = -1*NSHeight(coverRect);
	
	sourcesRect = leftPaneRect;
	sourcesRect.size.width = NSWidth(leftPaneRect)+1;
	
	if (animate) {
		[[sourcesView animator] setFrame:sourcesRect];
		[[coverView animator] setFrame:coverRect];
	} else {
		[sourcesView setFrame:sourcesRect];
		[coverView setFrame:coverRect];
	}
	
	[toggleCoverViewButton setImage:[NSImage imageNamed:@"Up.png"]];
}

- (void)addProcessActivityToQueue:(WBFSProcess *)aProcess {
	[queueController addProcessActivity:aProcess];
	[[queueController window] makeKeyAndOrderFront:self];
}

- (void)refreshQueue {
	//[queueController refresh];
	[queueController reloadProcessesActivity];
}



- (WBFSManagedDevice *)selectedDevice {
	id cell = [sourcesOutlineView selectedCell];
	if ([cell isKindOfClass:[WBFSManagedDevice class]]) {
		return cell;
	}
	return nil;
}

- (WBFSManagedPlace *)selectedPlace {
	id cell = [sourcesOutlineView selectedCell];
	if ([cell isKindOfClass:[WBFSManagedPlace class]]) {
		return cell;
	}
	return nil;
}

- (WBFSManagedDevice *)selectedSource {
	return [sourcesOutlineView itemAtRow:[sourcesOutlineView selectedRow]];
}

- (void)extractionPanelDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
	if (returnCode == NSAlertDefaultReturn) {
		char f = ([[filenameLayoutButton titleOfSelectedItem] rangeOfString:@"%/"].location == NSNotFound) ? 'f' : 'd';
		NSInteger i = [filenameLayoutButton indexOfSelectedItem]%3;
		NSString *filenameLayoutOption = [NSString stringWithFormat:@"%c%d", f, i];
		NSPasteboard* pboard = [(id < NSDraggingInfo >)contextInfo draggingPasteboard];
		NSData* rowData = [pboard dataForType:WBFSManagedGameDataType];
		NSIndexSet* rowIndexes = [NSKeyedUnarchiver unarchiveObjectWithData:rowData];
		NSArray *games = [gamesList arrangedObjects];
		[rowIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
			[self extractGame:[games objectAtIndex:idx] toPath:[dropDestination path] withFilenameLayout:filenameLayoutOption];
		}];
	}
	
}

- (void)alertApplicationTerminatePanelDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
	if (returnCode == NSAlertDefaultReturn) {
		[(NSApplication *)contextInfo replyToApplicationShouldTerminate: NO];
	} else {
		[(NSApplication *)contextInfo replyToApplicationShouldTerminate: YES];
	}

}

- (void)alertGamesDeletionPanelDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
	if (returnCode == NSAlertAlternateReturn) {
		if ([selectedSource isKindOfClass:[WBFSManagedDevice class]]) {
			NSUInteger i, count = [selectedGames count];
			float delSpace = 0.0;
			for (i = 0; i < count; i++) {
				WBFSManagedGame *game = [selectedGames objectAtIndex:i];
				WBFSProcess *rmProcess = [[WBFSProcess alloc] init];
				[rmProcess setCommand:kWBFSProcessCommandRemove];
				[rmProcess setSource:[selectedSource valueForKey:kDevicePathKey]];
				[rmProcess setGameID:[game valueForKey:kGameIDKey]];
				
				[rmProcess launch];
				[rmProcess release];
				
				[gamesList removeObject:game];
				
				delSpace += [[game sizeGB] floatValue];
			}
			[selectedSource updateInfoByFreeingSpace:delSpace];
			[devicesList rearrangeObjects];
		} else {
			NSUInteger i, count = [selectedGames count];
			for (i = 0; i < count; i--) {
				WBFSManagedGame *game = [selectedGames objectAtIndex:i];
				[[NSFileManager defaultManager] removeItemAtPath:game.path error:nil];
			}
			// Also remove selected games from the list
			[gamesList removeObjects:selectedGames];
		}
		[gamesList rearrangeObjects];
		[coverFlowView reloadData];
		
	}
}

- (NSMutableArray *)selectedGames {	
	[selectedGames removeAllObjects];
	NSIndexSet *indexes = [gamesList selectionIndexes];
	[indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
		WBFSManagedGame *game = [[gamesList arrangedObjects] objectAtIndex:idx];
		[selectedGames addObject:game];
	}];
	
	return selectedGames;
}

- (void)addGame:(WBFSGame *)game toDevice:(WBFSManagedDevice *)device {
	if ([device.totalGB floatValue] < 1.0) {
		[device updateInfo];
	}
	
	if ([game.sizeGB isGreaterThan:device.freeGB]) {
		NSRunCriticalAlertPanel(NSLocalizedString(@"NO_FREE_SPACE_TO_ADD_BOX_TITLE", @""), 
								NSLocalizedString(@"NO_FREE_SPACE_TO_ADD_MESSAGE", @""),
								NSLocalizedString(@"NO_FREE_SPACE_TO_ADD_BUTTON_TEXT", @""), nil, nil);
		return;
	}

	BOOL gameIndevice = ([[managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
																andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] %@ AND name == %@", device.path, game.name]] count] > 0);
	
	if (!gameIndevice) {
		WBFSProcess *proc = [[WBFSProcess alloc] initWithBinary:@"wwt"];
		[proc setCommand:@"add"];
		[proc setSource:game.path];
		NSString *dev = [device.path stringByReplacingOccurrencesOfString:@"/dev/" withString:@"/dev/r"];
		[proc setValue:dev forOption:@"--part"];
		[proc setOption:@"--progress"];
		proc.fullSize = [[game sizeGB] floatValue]*1024;
		[self addProcessActivityToQueue:proc];
		
		[game setPath:[device.path stringByAppendingPathComponent:game.gameId]];
		[WBFSManagedGame insertGame:game];
		
		[device updateInfoByUsingSpace:[game.sizeGB floatValue]];
	}	
}

- (void)extractGame:(WBFSGame *)game toPath:(NSString *)path withFilenameLayout:(NSString *)layout {
	// Check for space needed
	NSNumber *freeSize = [[[NSFileManager defaultManager] attributesOfFileSystemForPath:path error:nil] valueForKey:NSFileSystemFreeSize];
	if ([game.sizeGB isGreaterThan:freeSize]) {
		NSRunCriticalAlertPanel(NSLocalizedString(@"NO_FREE_SPACE_TO_EXTRACT_BOX_TITLE", @""), 
								NSLocalizedString(@"NO_FREE_SPACE_TO_EXTRACT_MESSAGE", @""),
								NSLocalizedString(@"NO_FREE_SPACE_TO_EXTRACT_BUTTON_TEXT", @""), nil, nil);
		return;
	}
	
	// Delete any temp file
	NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath: path];
	NSString *file;
	while ((file = [dirEnum nextObject])) {
		file = [path stringByAppendingPathComponent: file];
		if ([[file pathExtension] isEqualToString: @"tmp"]) {
			[[NSFileManager defaultManager] removeItemAtPath:file error: nil];
		}
	}
	
	// Build the process and add it to the queue
	WBFSProcess *extractProcess = [[WBFSProcess alloc] init];
	if ([game.format isEqualToString:@"ISO"])
		[extractProcess setCommand:kWBFSProcessCommandExtractISO];
	else
		[extractProcess setCommand:kWBFSProcessCommandExtractWBFS];
	[extractProcess setSource:[game.path stringByDeletingLastPathComponent]];
	[extractProcess setGameID:game.gameId];
	[extractProcess setDestination:path];
	[extractProcess setDestinationFilenameLayoutOption:layout];
	
	[self addProcessActivityToQueue:extractProcess];
}

- (void)convertGame:(WBFSGame *)game toPath:(NSString *)path withFilenameLayout:(NSString *)layout {
	// Check for space needed
	NSNumber *freeSize = [[[NSFileManager defaultManager] attributesOfFileSystemForPath:path error:nil] valueForKey:NSFileSystemFreeSize];
	if ([game.sizeGB isGreaterThan:freeSize]) {
		NSRunCriticalAlertPanel(NSLocalizedString(@"NO_FREE_SPACE_TO_CONVERT_BOX_TITLE", @""), 
								NSLocalizedString(@"NO_FREE_SPACE_TO_CONVERT_MESSAGE", @""),
								NSLocalizedString(@"NO_FREE_SPACE_TO_CONVERT_BUTTON_TEXT", @""), nil, nil);
		return;
	}
	
	// Delete any temp file
	NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: path error: nil];
	NSString *file;
	for (file in dirContents) {
		file = [path stringByAppendingPathComponent: file];
		if ([[file pathExtension] isEqualToString: @"tmp"]) {
			[[NSFileManager defaultManager] removeItemAtPath:file error: nil];
		}
	}
	
	// Build the process and add it to the queue
	WBFSProcess *convertProcess = [[WBFSProcess alloc] init];
	[convertProcess setCommand:kWBFSProcessCommandConvert];
	[convertProcess setSource:game.path];
	[convertProcess setDestination:path];
	[convertProcess setDestinationFilenameLayoutOption:layout];
	[convertProcess setOption:kWBFSProcessOptionDontSplitSize];
	[self addProcessActivityToQueue:convertProcess];
}

- (void)addPlaceWithPath:(NSString *)path {
	WBFSManagedPlace *place = [WBFSManagedPlace insertWithPath:path];
	[self addSource:place forGroup:@"PLACES"];		
	
	// Reload only PLACES group
	NSDictionary *placesGroup = [sourcesList objectAtIndex:2];
	[sourcesOutlineView reloadItem:placesGroup reloadChildren:YES];
	[sourcesOutlineView expandItem:placesGroup];
	// Select the place just added
	[sourcesOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[sourcesOutlineView rowForItem:place]] 
					byExtendingSelection:NO];
	[self performSelector:@selector(sourceSelected:)];
}

- (void)removePlace:(WBFSManagedPlace *)place {
	NSDictionary *placesGroup = [sourcesList objectAtIndex:2];
	[managedObjectContext deleteObject:place];
	NSMutableArray *places = [placesGroup valueForKey:kSourcesGroupListKey];
	[places removeObject:place];
	//Also remove the games
	[managedObjectContext deleteObjectsOfEntityWithName:kGameEntityName 
										   andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH %@", place.path]];
	
	// Reload only PLACES group
	[sourcesOutlineView reloadItem:placesGroup reloadChildren:YES];
	// Select My Games Library
	[sourcesOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:1] byExtendingSelection:NO];
}

- (void)removePlaylist:(WBFSManagedPlaylist *)playlist {
	NSDictionary *playlistsGroup = [sourcesList objectAtIndex:3];
	[managedObjectContext deleteObject:playlist];
	NSMutableArray *playlists = [playlistsGroup valueForKey:kSourcesGroupListKey];
	[playlists removeObject:playlist];
	
	// Reload only PLAYLISTS group
	[sourcesOutlineView reloadItem:playlistsGroup reloadChildren:YES];
	// Select My Games Library
	[sourcesOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:1] byExtendingSelection:NO];
}

- (void)addSource:(id)source forGroup:(NSString *)group {
	NSUInteger i, count = [sourcesList count];
	for (i = 0; i < count; i++) {
		NSDictionary *dict = [sourcesList objectAtIndex:i];
		if ([[dict valueForKey:kSourcesGroupNameKey] isEqualToString:group]) {
			NSMutableArray *array = [dict valueForKey:kSourcesGroupListKey];
			[array addObject:source];
		}
	}
}

- (void)removeSource:(id)source forGroup:(NSString *)group {
	NSUInteger i, count = [sourcesList count];
	for (i = 0; i < count; i++) {
		NSDictionary *dict = [sourcesList objectAtIndex:i];
		if ([[dict valueForKey:kSourcesGroupNameKey] isEqualToString:group]) {
			NSMutableArray *array = [dict valueForKey:kSourcesGroupListKey];
			[array removeObject:source];
		}
	}
}

- (void)setupWindowDisplay {
	NSRect leftRectPane = [leftPane bounds];
	NSRect listViewRect = [listView bounds];
	
	[gamesView setFrameOrigin:NSMakePoint(-1, 0)];
	[gamesView setFrameSize:NSMakeSize(NSWidth(listViewRect)+1, NSHeight(listViewRect))];
	[listView addSubview:gamesView];
	[sourcesView setFrameOrigin:NSMakePoint(0, 0)];
	[sourcesView setFrameSize:NSMakeSize(NSWidth(leftRectPane)+1, NSHeight(leftRectPane))];
	[leftPane addSubview:sourcesView];
	
	[coverView setFrameOrigin:NSMakePoint(0, -1*NSHeight([coverView frame]))];
	[coverView setFrameSize:NSMakeSize(NSWidth([leftPane bounds])+1, NSHeight([coverView frame]))];
	[leftPane addSubview:coverView];
}

- (void)updateWindowDisplay {
	NSRect leftRectPane = [leftPane bounds];
	NSRect listViewRect = [listView bounds];
	
	[gamesView setFrameOrigin:NSMakePoint(-1, 0)];
	[gamesView setFrameSize:NSMakeSize(NSWidth(listViewRect)+1, NSHeight(listViewRect))];
	
	[sourcesView setFrameOrigin:NSMakePoint(0, 0)];
	[sourcesView setFrameSize:NSMakeSize(NSWidth(leftRectPane)+1, NSHeight(leftRectPane))];
}

- (void)setupSourcesList {
	[sourcesOutlineView registerForDraggedTypes:
	 [NSArray arrayWithObjects: WBFSManagedGameDataType, NSFilenamesPboardType, nil]];
	
	// Build Sources List
	[managedObjectContext loadLibrary];
	
	sourcesList = [[NSMutableArray alloc] init];
	[sourcesList addObject:[NSDictionary dictionaryWithObjectsAndKeys:
							NSLocalizedString(@"LIBRARY_GROUP_NAME", @""), kSourcesGroupNameKey, 
							[NSArray arrayWithObject:NSLocalizedString(@"LIBRARY_MY_GAMES_NAME", @"")], kSourcesGroupListKey, nil]];
	
	//[managedObjectContext fillDevicesEntity];
	[sourcesList addObject:[NSDictionary dictionaryWithObjectsAndKeys:
							NSLocalizedString(@"DEVICES_GROUP_NAME", @""), kSourcesGroupNameKey, 
							[NSMutableArray arrayWithArray:[managedObjectContext fetchDevices]], kSourcesGroupListKey, nil]];
	
	[managedObjectContext loadPlaces];
	[sourcesList addObject:[NSDictionary dictionaryWithObjectsAndKeys:
							NSLocalizedString(@"PLACES_GROUP_NAME", @""), kSourcesGroupNameKey, 
							[NSMutableArray arrayWithArray:[managedObjectContext fetchPlaces]], kSourcesGroupListKey, nil]];
	
	[managedObjectContext loadPlaylists];
	[sourcesList addObject:[NSDictionary dictionaryWithObjectsAndKeys:
							NSLocalizedString(@"PLAYLISTS_GROUP_NAME", @""), kSourcesGroupNameKey, 
							[NSMutableArray arrayWithArray:[managedObjectContext fetchPlaylists]], kSourcesGroupListKey, nil]];
	
	
	[sourcesOutlineView reloadData];
	[sourcesOutlineView expandItem:[sourcesList objectAtIndex:0]];
	[sourcesOutlineView expandItem:[sourcesList objectAtIndex:1]];
	[sourcesOutlineView expandItem:[sourcesList objectAtIndex:2]];
	[sourcesOutlineView expandItem:[sourcesList objectAtIndex:3]];
	
	
	// Select My Games Library
	[sourcesOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:1] byExtendingSelection:NO];
	[self performSelector:@selector(sourceSelected:)];
}

- (void)setupGamesList {
	[gamesTableView registerForDraggedTypes:
	 [NSArray arrayWithObjects: NSFilenamesPboardType, NSFilesPromisePboardType, nil]];
	
	selectedGames = [[NSMutableArray arrayWithCapacity:0] retain];
	[gamesTableView reloadData];
}

- (void)setupCoverView {
	[coverTableView registerForDraggedTypes:
	 [NSArray arrayWithObjects: NSFilenamesPboardType, nil]];
	
	if ( coverViewOpened ) {
		[self openCoverViewWithAnimation: NO];
	}
}

- (void)setupCoverFlow {
	[coverFlowView setDelegate:self];
	[coverFlowView setDataSource:self];

	[coverFlowTypeControl setSelectedSegment:coverFlowType];
	
	[coverFlowView reloadData];
}

- (void)setupQuickLook {
	QLPreviewPanel *sharedPreviewPanel = [QLPreviewPanel sharedPreviewPanel];
	//[sharedPreviewPanel setDelegate:self];
	//[sharedPreviewPanel setDataSource:self];	
	
	[sharedPreviewPanel setContentView:previewPanelView];	
	//[sharedPreviewPanel setContentSize:[previewPanelView frame].size];
}

- (void)loadPrefs {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	coverViewOpened = [userDefaults boolForKey:kWBFSCoverViewOpenedPrefsKey];
	coverFlowViewOpened = [userDefaults boolForKey:kWBFSCoverFlowViewOpenedPrefsKey];
	coverFlowType = [userDefaults integerForKey:kWBFSCoverFlowTypePrefsKey];
}

- (void)savePrefs {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	[userDefaults setBool:coverViewOpened forKey:kWBFSCoverViewOpenedPrefsKey];
	[userDefaults setBool:coverFlowViewOpened forKey:kWBFSCoverFlowViewOpenedPrefsKey];
	[userDefaults setInteger:coverFlowType forKey:kWBFSCoverFlowTypePrefsKey];
	
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -
#pragma mark IBAction Methods

- (IBAction)togglePreviewPanel:(id)previewPanel {
    if ([QLPreviewPanel sharedPreviewPanelExists] && [[QLPreviewPanel sharedPreviewPanel] isVisible]) {
        [[QLPreviewPanel sharedPreviewPanel] orderOut:nil];
    } else {
        [[QLPreviewPanel sharedPreviewPanel] makeKeyAndOrderFront:nil];
    }
}

- (IBAction)addGames:(id)sender {
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	
	void (^handler)(NSInteger) = ^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
			NSUInteger i, count = [[panel URLs] count];
			for (i = 0; i < count; i++) {
				NSString *filePath = [[[panel URLs] objectAtIndex:i] path];
				WBFSGame *game = [[WBFSGame alloc] initWithFile:filePath];
				[self addGame: game toDevice: selectedSource];
				[game release];
			}
        }
    };
	
	[panel setMessage:NSLocalizedString(@"CHOOSE_FILEGAME", @"")];
	[panel setAllowsMultipleSelection:YES];
	[panel setCanChooseDirectories:NO];
	[panel setCanChooseFiles:YES];
	[panel setAllowedFileTypes:[NSArray arrayWithObjects:@"iso", @"wbfs", nil]];
	[panel beginSheetModalForWindow:window completionHandler:handler];
}

- (IBAction)deleteSelectedGames:(id)sender {
	if (![selectedSource isKindOfClass:[WBFSManagedPlaylist class]]) {
		// Show Alert to confirm deletion
		NSBeep();
		NSString *title;
		NSString *msg;
		NSUInteger n = [selectedGames count];
		if (n == 1) {
			title = NSLocalizedString(@"DELETE_SINGLE_BOX_TITLE", @"");
			msg = NSLocalizedString(@"DELETE_SINGLE_CONFIRMATION_MESSAGE", @"");
			msg = [NSString stringWithFormat: msg, [[selectedGames objectAtIndex:0] name]];
		} else {
			title = NSLocalizedString(@"DELETE_MULTI_BOX_TITLE", @"");
			msg = NSLocalizedString(@"DELETE_MULTI_CONFIRMATION_MESSAGE", @"");
			msg = [NSString stringWithFormat: msg, n];
		}
		
		NSBeginAlertSheet(title, 
						  NSLocalizedString(@"DONT_DELETE_BUTTON_TEXT", @""), 
						  NSLocalizedString(@"DELETE_BUTTON_TEXT", @""), nil, 
						  window, self, 
						  @selector(alertGamesDeletionPanelDidEnd:returnCode:contextInfo:), 
						  nil, sender, 
						  msg);
	} else {
		NSMutableArray *gamesArray = [NSMutableArray arrayWithArray:[[selectedSource games] allObjects]];
		[gamesArray removeObjectsInArray:selectedGames];
		[selectedSource setGames:[NSSet setWithArray:gamesArray]];
		[gamesList setContent:gamesArray];
		[gamesList rearrangeObjects];
		[coverFlowView reloadData];
	}

}

- (IBAction)extractSelectedGames:(id)sender {
	if ([selectedSource isKindOfClass:[WBFSManagedDevice class]]) {
		NSOpenPanel *panel = [NSOpenPanel openPanel];
	
		void (^handler)(NSInteger) = ^(NSInteger result) {
			if (result == NSFileHandlingPanelOKButton) {
				NSUInteger i, count = [[panel URLs] count];
				for (i = 0; i < count; i++) {
					NSString *folderPath = [[[panel URLs] objectAtIndex:i] path];
					NSUInteger idx, count = [selectedGames count];
					NSString *format = [[formatButton titleOfSelectedItem] lowercaseString];
					char f = ([[filenameLayoutButton titleOfSelectedItem] rangeOfString:@"%/"].location == NSNotFound) ? 'f' : 'd';
					NSInteger i = [filenameLayoutButton indexOfSelectedItem]%3;
					NSString *filenameLayoutOption = [NSString stringWithFormat:@"%c%d", f, i];
					for (idx = 0; idx < count; idx++) {
						NSManagedObject *game = [selectedGames objectAtIndex:idx];
						WBFSProcess *addProcess = [[WBFSProcess alloc] init];
						if ([format isEqualToString:@"ISO"])
							[addProcess setCommand:kWBFSProcessCommandExtractISO];
						else
							[addProcess setCommand:kWBFSProcessCommandExtractWBFS];
						[addProcess setSource:[selectedSource valueForKey:kDevicePathKey]];
						[addProcess setGameID:[game valueForKey:kGameIDKey]];
						[addProcess setDestination:folderPath];
						[addProcess setDestinationFilenameLayoutOption:filenameLayoutOption];
						
						[self addProcessActivityToQueue:addProcess];
						[[queueController window] makeKeyAndOrderFront:self];
                        [addProcess release];
						//[addProcess launch];
						//[[parentController queueController] reloadProcessesActivity];
					}
					
				}
			}
		};
		
		[panel setMessage:NSLocalizedString(@"CHOOSE_DESTINATION_FOLDER", @"")];
		[panel setAllowsMultipleSelection:NO];
		[panel setCanChooseDirectories:YES];
		[panel setCanChooseFiles:NO];
		
		[panel setAccessoryView:accessoryView];
		[panel beginSheetModalForWindow:window completionHandler:handler];
	}
	else {
		[self performSelector:@selector(convertSelectedGames:)];
	}
	
	
}

- (IBAction)convertSelectedGames:(id)sender {
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	
	void (^handler)(NSInteger) = ^(NSInteger result) {
		if (result == NSFileHandlingPanelOKButton) {
			NSUInteger i, count = [[panel URLs] count];
			for (i = 0; i < count; i++) {
				NSString *folderPath = [[[panel URLs] objectAtIndex:i] path];
				NSUInteger idx, count = [selectedGames count];
				char f = ([[filenameLayoutButton titleOfSelectedItem] rangeOfString:@"%/"].location == NSNotFound) ? 'f' : 'd';
				NSInteger i = [filenameLayoutButton indexOfSelectedItem]%3;
				NSString *filenameLayoutOption = [NSString stringWithFormat:@"%c%d", f, i];
				for (idx = 0; idx < count; idx++) {
					WBFSManagedGame *aGame = [selectedGames objectAtIndex: idx];
					WBFSGame *game = [[WBFSGame alloc] initWithFile: aGame.path];
					[self convertGame: game
							   toPath: folderPath 
				   withFilenameLayout: filenameLayoutOption];
					[game release];
				}
			}
		}
	};
	
	[panel setMessage:@"Choose destination folder"];
	[panel setAllowsMultipleSelection:NO];
	[panel setCanChooseDirectories:YES];
	[panel setCanChooseFiles:NO];
	
	[panel setAccessoryView:accessoryView];
	[panel beginSheetModalForWindow:window completionHandler:handler];
	
}

- (IBAction)extractAll:(id)sender {
	
}

- (IBAction)sourceSelected:(id)sender {
	selectedSource = [self selectedSource];
	[devicesList setSelectedObjects:nil];
	if ([selectedSource isKindOfClass:[NSString class]] && [selectedSource isEqualToString:NSLocalizedString(@"LIBRARY_MY_GAMES_NAME", @"")]) {
		[gamesList setContent:[managedObjectContext fetchGamesInLibrary]];
	} else if ([selectedSource isKindOfClass:[WBFSManagedPlace class]]) {
		[gamesList setContent:[managedObjectContext fillGamesEntityWithPlace:selectedSource]];
	} else if ([selectedSource isKindOfClass:[WBFSManagedDevice class]]) {
		[gamesList setContent:[managedObjectContext fillGamesEntityWithDevice:selectedSource]];
		[devicesList setSelectedObjects:[NSArray arrayWithObject:selectedSource]];
	} else if ([selectedSource isKindOfClass:[WBFSManagedPlaylist class]]) {
		WBFSManagedPlaylist *playlist = (WBFSManagedPlaylist *)selectedSource;
		if (playlist.predicate == nil) {
			[gamesList setContent:[[selectedSource games] allObjects]];
		} else {
			[gamesList setContent:[managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
																		andPredicate:[NSPredicate predicateWithFormat:playlist.predicate]]];
		}
	}
	[coverFlowView reloadData];
}

- (IBAction)gamesSelected:(id)sender {
	[self selectedGames];
}

- (IBAction)formatSelectedDevice:(id)sender {
	NSAlert *confirm = [NSAlert alertWithMessageText:@"Do you want to format this drive" 
									   defaultButton:@"Format" 
									 alternateButton:@"Don't format" 
										 otherButton:nil 
						   informativeTextWithFormat:@"All data include in it will be lost"];
	
	[confirm beginSheetModalForWindow:window
						modalDelegate:self 
					   didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) 
						  contextInfo:(void *)1];
}

- (IBAction)reloadSelectedDeviceStatus:(id)sender {
	[devicesList rearrangeObjects];
}

- (IBAction)downloadAllCovers:(id)sender {
	NSArray *games = [gamesList arrangedObjects];
	NSUInteger i, count = [games count];
	for (i = 0; i < count; i++) {
		WBFSManagedGame *game = [games objectAtIndex:i];
		[game imageCover3D];
		[game imageCover];
		[game imageCoverfull];
		[game imageCoverfullHQ];
		[game imageDisc];
	}
}

- (IBAction)coverFlowTypeChanged:(id)sender {
	coverFlowType = [(NSSegmentedControl *)sender selectedSegment];
	[managedObjectContext clearCachedCoversView];
	[coverFlowView reloadData];
	[coverTableView reloadData];
}

- (IBAction)toggleCoverFlowView:(id)sender {
	NSRect r = [rightPane bounds];
	if ( coverFlowViewOpened ) {
		[coverFlowView setFrameSize:NSMakeSize(0, 0)];
	}
	else {
		[coverFlowView setFrameSize:NSMakeSize(NSWidth(r), 1000)];
	}
	
	coverFlowViewOpened = !coverFlowViewOpened;
}

- (IBAction)toggleCoverView:(id)sender {	
	// If the view is already opened, so close it else open it
	if ( coverViewOpened ) {
		[self closeCoverViewWithAnimation: YES];
	} else {
		[self openCoverViewWithAnimation: YES];
	}
	
	coverViewOpened = !coverViewOpened;
}

- (IBAction)addPlace:(id)sender {
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	
	void (^handler)(NSInteger) = ^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
			NSUInteger i, count = [[panel URLs] count];
			for (i = 0; i < count; i++) {
				NSString *folderPath = [[[panel URLs] objectAtIndex:i] path];
				[self addPlaceWithPath:folderPath];
			}
        }
    };
	
	[panel setMessage:NSLocalizedString(@"CHOOSE_SOURCE_FOLDER", @"")];
	[panel setAllowsMultipleSelection:YES];
	[panel setCanChooseDirectories:YES];
	[panel setCanChooseFiles:NO];
	
	[panel beginSheetModalForWindow:window completionHandler:handler];
	
	
}

- (IBAction)addPlaylist:(id)sender {
	 WBFSManagedPlaylist *playlist = [WBFSManagedPlaylist insertWithName:@"New list"];
	 [playlist setGames:[NSSet setWithArray:selectedGames]];
	 [self addSource:playlist forGroup:NSLocalizedString(@"PLAYLISTS_GROUP_NAME", @"")];		
	 
	 // Reload only PLAYLIST group
	 NSDictionary *playlistGroup = [sourcesList objectAtIndex:3];
	 [sourcesOutlineView reloadItem:playlistGroup reloadChildren:YES];
	 [sourcesOutlineView expandItem:playlistGroup];
	 NSInteger playlistRow = [sourcesOutlineView rowForItem:playlist];
	 [sourcesOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:playlistRow] byExtendingSelection:NO];
	 [sourcesOutlineView editColumn:0 row:playlistRow withEvent:nil select:YES];
}

- (IBAction)addSmartPlaylist:(id)sender {
	[predicate release];
	predicate = [[self predicateForNewSmartPlaylist] retain];
	[smartPlaylistWindow makeKeyAndOrderFront:sender];
}

- (IBAction)saveSmartPlaylist:(id)sender {
	WBFSManagedPlaylist *playlist = [WBFSManagedPlaylist insertWithName:@"New smart list"];
	playlist.predicate = [predicate description];
	[self addSource:playlist forGroup:NSLocalizedString(@"PLAYLISTS_GROUP_NAME", @"")];
	
	// Reload only PLAYLIST group
	NSDictionary *playlistGroup = [sourcesList objectAtIndex:3];
	[sourcesOutlineView reloadItem:playlistGroup reloadChildren:YES];
	[sourcesOutlineView expandItem:playlistGroup];
	NSInteger playlistRow = [sourcesOutlineView rowForItem:playlist];
	[sourcesOutlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:playlistRow] byExtendingSelection:NO];
	[sourcesOutlineView editColumn:0 row:playlistRow withEvent:nil select:YES];
	
	[gamesList setContent:[managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
																andPredicate:predicate]];

	[coverFlowView reloadData];
	
	[smartPlaylistWindow orderOut:sender];
}

- (IBAction)removeSelectedSource:(id)sender {
	id source = [self selectedSource];
	
	if ([source isKindOfClass:[WBFSManagedPlace class]]) {
		[self removePlace:source];
	} else if ([source isKindOfClass:[WBFSManagedPlaylist class]]) {
		[self removePlaylist:source];
	}
}

- (IBAction)test:(id)sender {
	[self performSelector:@selector(addSmartPlaylist:)];
}

#pragma mark -
#pragma mark FileSystem Functions

- (void)initializeEventStream
{
	NSArray *places = [managedObjectContext fetchPlaces];
    NSMutableArray *pathsToWatch = [NSMutableArray arrayWithCapacity:0];//[NSArray arrayWithObject:myPath];
	for (WBFSManagedPlace *place in places) {
		[pathsToWatch addObject:place.path];
	}
    void *appPointer = (void *)self;
    FSEventStreamContext context = {0, appPointer, NULL, NULL, NULL};
    NSTimeInterval latency = 3.0;
    stream = FSEventStreamCreate(NULL,
                                 &fsevents_callback,
                                 &context,
                                 (CFArrayRef) pathsToWatch,
                                 [lastEventId unsignedLongLongValue],
                                 (CFAbsoluteTime) latency,
                                 kFSEventStreamCreateFlagUseCFTypes
								 );
	
    FSEventStreamScheduleWithRunLoop(stream,
                                     CFRunLoopGetCurrent(),
                                     kCFRunLoopDefaultMode);
    FSEventStreamStart(stream);
}

void fsevents_callback(ConstFSEventStreamRef streamRef,
                       void *userData,
                       size_t numEvents,
                       void *eventPaths,
                       const FSEventStreamEventFlags eventFlags[],
                       const FSEventStreamEventId eventIds[])
{
    WBFSManagerAppDelegate *ac = (WBFSManagerAppDelegate *)userData;
    size_t i;
    for(i=0; i < numEvents; i++) {
        NSString *file = [(NSArray *)eventPaths objectAtIndex:i];
		NSArray *places = [ac.managedObjectContext fetchPlaceForPath:[file substringToIndex:[file length]-1]];
		if ( [places count]>0 ) {
			/*WBFSManagedPlace *place = [places objectAtIndex:0];
			[ac.gamesList setContent: [ac.managedObjectContext fillGamesEntityWithPlace:place]];
			[ac.coverFlowView reloadData];*/
			NSLog(@"%@", file);
		}
		
    }
	
}

#pragma mark -
#pragma mark NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
	return ([selectedGames count]) ? 1 : 0;
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex {
	[self selectedGames];
	if ( [selectedGames count]>0 ) {
		WBFSManagedGame *game = [selectedGames objectAtIndex:0];
		NSImage *cover = [game imageRepresentation];
		return cover;
	}	
	//return [[[NSImage alloc] init] autorelease];
	return [[NSImage alloc] init];
}


#pragma mark -
#pragma mark NSTableViewDelegate

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)rowIndex {
	return !(tableView==coverTableView);
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification {
	if ( coverViewOpened ) {
		[coverTableView reloadData];
	}
	if ( [selectedGames count]>0 ) {
		[coverFlowView setSelectedIndex:[[gamesTableView selectedRowIndexes] firstIndex]];
	}
}
/*
- (void)tableView:(NSTableView *)aTableView sortDescriptorsDidChange:(NSArray *)oldDescriptors {
	[coverFlowView reloadData];
}
*/
- (BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard
{
    // Copy the row numbers to the pasteboard.
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:rowIndexes];
    [pboard declareTypes:[NSArray arrayWithObject:WBFSManagedGameDataType] owner:self];
    [pboard setData:data forType:WBFSManagedGameDataType];
    //[pboard declareTypes:[NSArray arrayWithObject:NSFilesPromisePboardType] owner:self];
    //[pboard setData:data forType:NSFilesPromisePboardType];
    return YES;
}

- (NSDragOperation)tableView:(NSTableView*)tableView validateDrop:(id <NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)op
{
    NSPasteboard* pboard = [info draggingPasteboard];
	if (tableView == gamesTableView) {
		if ( [[pboard types] containsObject:NSFilenamesPboardType] ) {
			NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
			NSString *filepath = [files objectAtIndex:0];
			if ( ([[filepath pathExtension] isEqualToString:@"iso"] || [[filepath pathExtension] isEqualToString:@"wbfs"])
				&& ([selectedSource isKindOfClass:[WBFSManagedDevice class]] || [selectedSource isKindOfClass:[WBFSManagedPlace class]]) 
				&& op == NSTableViewDropAbove ) {
				// Validate a drag of games (files) in a games list of a device
				return NSDragOperationCopy;
			}
		}
	} else if (tableView == coverTableView) {
		NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
		NSString *filepath = [files objectAtIndex:0];
		FSRef ref;
		FSPathMakeRef( (const UInt8 *)[filepath fileSystemRepresentation], &ref, NULL );
		CFTypeRef itemUTI = NULL;
		LSCopyItemAttribute( &ref, kLSRolesAll, kLSItemContentType, &itemUTI );
		if ( [selectedGames count] > 0 
			&& (UTTypeConformsTo((CFStringRef)itemUTI, CFSTR("public.jpeg")) || UTTypeConformsTo((CFStringRef)itemUTI, CFSTR("public.png")) )
			&& op == NSTableViewDropOn ) {
			// Validate a drag of an image in cover view
			return NSDragOperationEvery;
		}
	}
	// Denied all otherr drag
	return NSDragOperationNone;
}

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id <NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)operation
{
	NSPasteboard* pboard = [info draggingPasteboard];
	NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
	if (tableView == coverTableView) {
		// It is a drop in cover view, so update the cover of the selected game with the image
		NSString *filepath = [files objectAtIndex:0];
		[[selectedGames objectAtIndex:0] setImage:[[[NSImage alloc] initWithContentsOfFile:filepath] autorelease]];
		[coverFlowView reloadData];
		return YES;
	} else {
		// It is a drop in games list, so we add the files
		for (NSString *filepath in files) {
			WBFSGame *game = [[WBFSGame alloc] initWithFile:filepath];
			[WBFSManagedGame insertGame: game];
			[self addGame:game toDevice:selectedSource];
			[game release];
		}
		return YES;
	}

	return NO;
}

- (NSArray *)tableView:(NSTableView *)tableView namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination forDraggedRowsWithIndexes:(NSIndexSet *)indexSet {
	return [NSArray array];
}

#pragma mark -
#pragma mark NSOutlineViewDataSource

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
	if (outlineView == coverTableView)
		return 1;
		
	if (item == nil) {
		return [sourcesList count];
	}
	NSArray *groupList = [item valueForKey:kSourcesGroupListKey];
	return [groupList count];
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
	if (outlineView == coverTableView)
		return [selectedGames objectAtIndex:0];
	
	if (item == nil) {
		return [sourcesList objectAtIndex:index];
	}
	NSArray *groupList = [item valueForKey:kSourcesGroupListKey];
	return [groupList objectAtIndex:index];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
	if (outlineView == coverTableView)
		return NO;
		
	if ([item isKindOfClass:[NSDictionary class]]) {
		NSArray *groupList = [item valueForKey:kSourcesGroupListKey];
		return [groupList count];
	}
	return NO;
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
	if (outlineView == coverTableView) {
		WBFSManagedGame *game = [selectedGames objectAtIndex:0];
		NSImage *cover = [game imageRepresentation];
		return cover;
	}
	if ([item isKindOfClass:[NSDictionary class]]) {
		return [item valueForKey:kSourcesGroupNameKey];
	} else if ([item isKindOfClass:[WBFSManagedDevice class]] || [item isKindOfClass:[WBFSManagedPlace class]] || [item isKindOfClass:[WBFSManagedPlaylist class]]) {
		return [item name];
	}
	return item;
}

- (void)outlineView:(NSOutlineView *)outlineView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	if ([item isKindOfClass:[NSString class]] && [item isEqualToString:NSLocalizedString(@"LIBRARY_MY_GAMES_NAME", @"")]) {
		NSImage *image = [NSImage imageNamed:@"my_games.tiff"];
		[image setSize:NSMakeSize(16, 16)];
		[cell setImage:image];
	} else if ([item isKindOfClass:[WBFSManagedDevice class]] || [item isKindOfClass:[WBFSManagedPlace class]]) {
		NSImage *image = [item icon];
		[image setSize:NSMakeSize(16, 16)];
		[cell setImage:image];
	} else if ([item isKindOfClass:[WBFSManagedPlaylist class]]) {
		NSImage *image;
		if ([item predicate] == nil) {
			image = [NSImage imageNamed:@"playlist.tiff"];
		} else {
			image = [NSImage imageNamed:@"smartplaylist.tiff"];
		}
		[image setSize:NSMakeSize(16, 16)];
		[cell setImage:image];
	}
}

// We can return a different cell for each row, if we want
- (NSCell *)outlineView:(NSOutlineView *)outlineView dataCellForTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	if (outlineView == coverTableView) {
		//return [[[NSImageCell alloc] init] autorelease];
        return [[NSImageCell alloc] init];
	}
    
	if (![item isKindOfClass:[NSDictionary class]]) {
		//return [[[ImageAndTextCell alloc] init] autorelease];
		return [[ImageAndTextCell alloc] init];
	}
	return [tableColumn dataCell];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item {
	if (outlineView == coverTableView)
		return NO;
	
	return [item isKindOfClass:[NSDictionary class]];
}

- (void)outlineView:(NSOutlineView *)outlineView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
	[(WBFSManagedPlaylist *)item setName:object];
}

#pragma mark -
#pragma mark NSOutlineViewDelegate

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item {
	return ![self outlineView:outlineView isGroupItem:item];
}

- (void)outlineViewSelectionDidChange:(NSNotification *)notification {
	[coverTableView reloadData];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pboard {
    if ([[items objectAtIndex:0] isKindOfClass:[WBFSManagedPlace class]] || [[items objectAtIndex:0] isKindOfClass:[WBFSManagedPlaylist class]]) {
		[(WBFSSourcesOutlineView *)outlineView setDraggedItems:items];
		return YES;
	}
	return NO;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	return ([item isKindOfClass:[WBFSManagedPlaylist class]]) ? YES : NO;
}

- (NSDragOperation)outlineView:(NSOutlineView *)outlineView validateDrop:(id < NSDraggingInfo >)info proposedItem:(id)item proposedChildIndex:(NSInteger)index {
	if (outlineView == coverTableView)
		return NSDragOperationEvery;
	
	NSPasteboard* pboard = [info draggingPasteboard];
	if ( [[pboard types] containsObject:NSFilenamesPboardType] ) {
        NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
		NSString *filepath = [files objectAtIndex:0];
		BOOL isDirectory = NO;
		[[NSFileManager defaultManager] fileExistsAtPath:filepath isDirectory:&isDirectory];
		if ( [files count] == 1 && isDirectory 
			&& ((index == -1 && item == nil) || (index != -1 && [item isKindOfClass:[NSDictionary class]] && [[item valueForKey:kSourcesGroupNameKey] isEqualToString:@"PLACES"])) ) {
			// Validate a drag of a folder in PLACES section
			return NSDragOperationLink;
		} else if ( ([[filepath pathExtension] isEqualToString:@"iso"] || [[filepath pathExtension] isEqualToString:@"wbfs"]) 
				   && ([item isKindOfClass:[WBFSManagedDevice class]] || [item isKindOfClass:[WBFSManagedPlace class]]) ) {
			// Validate a drag of game (files) in a device or a place
			return NSDragOperationCopy;
		} 
	} else if ( [[pboard types] containsObject:WBFSManagedGameDataType] ) {
		if ( [selectedSource isKindOfClass:[WBFSManagedDevice class]] && [item isKindOfClass:[WBFSManagedPlace class]] ) {
			// Validate a drag of a game (device) in a place
			return NSDragOperationCopy;
		} else if ( ![selectedSource isKindOfClass:[WBFSManagedDevice class]] && ([item isKindOfClass:[WBFSManagedDevice class]]
																				  || ([item isKindOfClass:[WBFSManagedPlaylist class]] && [(WBFSManagedPlaylist *)item predicate] == nil))) {
			// Validate a drag of a game in a device or playlist (but not smart)
			return NSDragOperationCopy;
		}
		
	}
	// Denied all other drag			   
	return NSDragOperationNone;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView acceptDrop:(id < NSDraggingInfo >)info item:(id)item childIndex:(NSInteger)index {
	NSPasteboard* pboard = [info draggingPasteboard];
	if ( [[pboard types] containsObject:NSFilenamesPboardType] ) {
        NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
		if ([files count]==1 && item==nil) {
			// It is a drop of a folder so we add the new place
			NSString *filepath = [files objectAtIndex:0];
			[self addPlaceWithPath:filepath];
		} else if ([item isKindOfClass:[WBFSManagedPlace class]]) {
			// It is a drop of file(s) into a place so we copy the file(s)
			for (NSString *filepath in files) {
				NSString *newPath = [[item path] stringByAppendingPathComponent:[filepath lastPathComponent]];
				if ([[NSFileManager defaultManager] copyItemAtPath:filepath toPath:newPath error:nil]) {
					WBFSManagedGame *game = [[WBFSManagedGame alloc] initWithFile:newPath];
					[gamesList addObject:game];
				}
				
			}
		} else if ([item isKindOfClass:[WBFSManagedDevice class]]) {
			// It is a drop of a file(s) on a device so we add the new game(s)
			for (NSString *filepath in files) {
				WBFSGame *game = [[WBFSGame alloc] initWithFile:filepath];
				[self addGame:game toDevice:item];
				[WBFSManagedGame insertGame:game];
				[game release];
			}
		}
		return YES;
	} else if ( [[pboard types] containsObject:WBFSManagedGameDataType] ) {
		if ([item isKindOfClass:[WBFSManagedDevice class]]) {
			// It is a drop of game(s) from the library into a device so we add the game(s)
			NSData* rowData = [pboard dataForType:WBFSManagedGameDataType];
			NSIndexSet* rowIndexes = [NSKeyedUnarchiver unarchiveObjectWithData:rowData];
			[rowIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
				WBFSManagedGame *dropGame = [[gamesList arrangedObjects] objectAtIndex:idx];
				WBFSGame *game = [[WBFSGame alloc] initWithFile:dropGame.path];
				[self addGame:game toDevice:item];
				[game release];
			}];
		} else if ([item isKindOfClass:[WBFSManagedPlace class]]) {
			// It is a drop of game(s) from a device into a place so we extract the game(s)
			dropDestination = item;
			NSAlert *extractionPanel = [NSAlert alertWithMessageText:@"Extraction Options" 
													   defaultButton:@"Extract" 
													 alternateButton:@"Cancel" 
														 otherButton:nil 
										   informativeTextWithFormat:@"Choose a format and a an ouptut filname layout"];
			[extractionPanel setAccessoryView:accessoryView];
			[extractionPanel beginSheetModalForWindow:window
										modalDelegate:self 
									   didEndSelector:@selector(extractionPanelDidEnd:returnCode:contextInfo:) 
										  contextInfo:info];
		} else if ([item isKindOfClass:[WBFSManagedPlaylist class]]) {
			// It is a drop of game(s) from the library into a playlist so we link the game(s)
			NSMutableSet *gamesSet = (NSMutableSet *)[item games];
			NSData* rowData = [pboard dataForType:WBFSManagedGameDataType];
			NSIndexSet* rowIndexes = [NSKeyedUnarchiver unarchiveObjectWithData:rowData];
			[rowIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
				WBFSManagedGame *dropGame = [[gamesList arrangedObjects] objectAtIndex:idx];
				[gamesSet addObject:dropGame];
			}];
			
			[item setGames:gamesSet];
		}
		return YES;
	}
	
	return NO;
}


#pragma mark -
#pragma mark NSSplitViewDelegate

- (void)splitViewDidResizeSubviews:(NSNotification *)notification {
	[self updateWindowDisplay];
}

#pragma mark -
#pragma mark NSWindowDelegate

- (void)windowDidResize:(NSNotification *)notification {
	[self updateWindowDisplay];
}

#pragma mark -
#pragma mark IKImageFlowViewDataSource

- (NSUInteger)numberOfItemsInImageFlow:(IKImageFlowView *)sender
{
	//NSLog(@"numberOfItemsInImageFlow:");
	//NSLog(@"count=%@",[[gamesList content] count]);
	return [[gamesList arrangedObjects] count];
}

- (id)imageFlow:(IKImageFlowView *)sender itemAtIndex:(NSInteger)index
{
	//NSLog(@"imageFlow:itemAtIndex:");
	//NSLog(@"array=%@",[gamesList content]);
	return [[gamesList arrangedObjects] objectAtIndex:index];
}

#pragma mark -
#pragma mark IKImageFlowViewDelegate

- (void)imageFlow:(IKImageFlowView *)sender didSelectItemAtIndex:(NSInteger)index
{
	[gamesTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index] byExtendingSelection:NO];
}

#pragma mark -
#pragma mark Notifications Methods

-(void)diskDidAppeared:(NSNotification *)notification {
	CFDictionaryRef dict = (CFDictionaryRef)[notification userInfo];
	CFStringRef name = (CFStringRef)CFDictionaryGetValue(dict, kDeviceNameKey);	
	CFStringRef dev = (CFStringRef)CFDictionaryGetValue(dict, kDevicePathKey);
	
	WBFSManagedDevice *device = [WBFSManagedDevice insertWithPath:[NSString stringWithFormat:@"%@", dev]
														  forName:[NSString stringWithFormat:@"%@", name]];
	[self addSource:device forGroup:NSLocalizedString(@"DEVICES_GROUP_NAME", @"")];		
	
	// Reload only DEVICES group
	NSDictionary *devicesGroup = [sourcesList objectAtIndex:1];
	[sourcesOutlineView reloadItem:devicesGroup reloadChildren:YES];
	[sourcesOutlineView expandItem:devicesGroup];
}

-(void)diskDidDisappeared:(NSNotification *)notification {
	CFDictionaryRef dict = (CFDictionaryRef)[notification userInfo];	
	CFStringRef dev = (CFStringRef)CFDictionaryGetValue(dict, kDevicePathKey);
	
	WBFSManagedDevice *device = [[managedObjectContext fetchDeviceForPath:(NSString *)dev] objectAtIndex:0];
	[self removeSource:device forGroup:NSLocalizedString(@"DEVICES_GROUP_NAME", @"")];		
	
	// Reload only DEVICES group
	NSDictionary *devicesGroup = [sourcesList objectAtIndex:1];
	[sourcesOutlineView reloadItem:devicesGroup reloadChildren:YES];
	[sourcesOutlineView expandItem:devicesGroup];
}

#pragma mark -
#pragma mark NSApplicationDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
	WBFSSetupDisksNotification();
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(diskDidAppeared:) name:@"WBFSDiskAppeared" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(diskDidDisappeared:) name:@"WBFSDiskDisappeared" object:nil];
		
	//[self initializeEventStream];
	
	[coverTableView reloadData];

}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
	if ([queueController isRunning]) {
		[queueController showWindow: sender];
		NSBeginAlertSheet(NSLocalizedString(@"QUIT_BOX_TITLE", @""), 
						  NSLocalizedString(@"DONT_QUIT_BUTTON_TEXT", @""), 
						  NSLocalizedString(@"QUIT_ANYWAY_BUTTON_TEXT", @""), nil, 
						  window, self, 
						  @selector(alertApplicationTerminatePanelDidEnd:returnCode:contextInfo:), 
						  nil, sender, 
						  NSLocalizedString(@"QUIT_ANYWAY_CONFIRMATION_MESSAGE", @""));
		return NSTerminateLater;
	}
	
	return NSTerminateNow;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	[managedObjectContext saveLibrary];
	[managedObjectContext savePlaces];
	[managedObjectContext savePlaylists];
	
	FSEventStreamStop(stream);
    FSEventStreamInvalidate(stream);
	
	[self savePrefs];
	
	[WBFSDatabase loadDatabase];
}

- (void)application:(NSApplication *)sender openFiles:(NSArray *)filenames {
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	
	void (^handler)(NSInteger) = ^(NSInteger result) {
		if (result == NSFileHandlingPanelOKButton) {
			NSUInteger i, count = [[panel URLs] count];
			for (i = 0; i < count; i++) {
				NSString *folderPath = [[[panel URLs] objectAtIndex:i] path];
				NSUInteger idx, count = [filenames count];
				char f = ([[filenameLayoutButton titleOfSelectedItem] rangeOfString:@"%/"].location == NSNotFound) ? 'f' : 'd';
				NSInteger i = [filenameLayoutButton indexOfSelectedItem]%3;
				NSString *filenameLayoutOption = [NSString stringWithFormat:@"%c%d", f, i];
				for (idx = 0; idx < count; idx++) {
					NSString *gamePath = [filenames objectAtIndex:idx];
					WBFSGame *game = [[WBFSGame alloc] initWithFile:gamePath];
					[self convertGame: game 
							   toPath: folderPath 
				   withFilenameLayout:filenameLayoutOption];
					[game release];
				}
			}
		}
	};
	
	[panel setMessage:@"Choose destination folder"];
	[panel setAllowsMultipleSelection:NO];
	[panel setCanChooseDirectories:YES];
	[panel setCanChooseFiles:NO];
	
	[panel setAccessoryView:accessoryView];
	[panel beginSheetModalForWindow:window completionHandler:handler];
}

#pragma mark -
#pragma mark QLPreviewPanelDataSource

- (NSInteger)numberOfPreviewItemsInPreviewPanel:(QLPreviewPanel *)panel {
	return [[gamesList arrangedObjects] count];
}

- (id <QLPreviewItem>)previewPanel:(QLPreviewPanel *)panel previewItemAtIndex:(NSInteger)index {
	WBFSManagedGame *item = [selectedGames objectAtIndex:0];
	
	[previewPanelImageView setImage:item.image];
	[previewPanelImageView setNeedsDisplay:YES];
	
	return item;
}

#pragma mark -
#pragma mark QLPreviewPanelDelegate

- (BOOL)acceptsPreviewPanelControl:(QLPreviewPanel *)panel;
{
    return YES;
}

- (void)beginPreviewPanelControl:(QLPreviewPanel *)panel
{
    // This document is now responsible of the preview panel
    // It is allowed to set the delegate, data source and refresh panel.
    previewPanelView = [panel retain];
    panel.delegate = self;
    panel.dataSource = self;
	//[panel setContentView:previewPanelView];
}

- (void)endPreviewPanelControl:(QLPreviewPanel *)panel
{
    // This document loses its responsisibility on the preview panel
    // Until the next call to -beginPreviewPanelControl: it must not
    // change the panel's delegate, data source or refresh it.
    [previewPanelView release];
    previewPanelView = nil;
}

- (BOOL)previewPanel:(QLPreviewPanel *)panel handleEvent:(NSEvent *)event
{
    // redirect all key down events to the table view
    if ([event type] == NSKeyDown) {
        [gamesTableView keyDown:event];
		[[QLPreviewPanel sharedPreviewPanel] reloadData];
        return YES;
    }
    return NO;
}

// This delegate method provides the rect on screen from which the panel will zoom.
- (NSRect)previewPanel:(QLPreviewPanel *)panel sourceFrameOnScreenForPreviewItem:(id <QLPreviewItem>)item
{
    NSInteger index = [gamesTableView selectedRow];
    if (index == NSNotFound) {
        return NSZeroRect;
    }
	
    NSRect rowRect = [gamesTableView rectOfRow:index];
    
    // check that the row rect is visible on screen
    NSRect visibleRect = [gamesTableView visibleRect];
    
    if (!NSIntersectsRect(visibleRect, rowRect)) {
        return NSZeroRect;
    }
    
    // convert row rect to screen coordinates
    rowRect = [gamesTableView convertRectToBase:rowRect];
    rowRect.origin = [[gamesTableView window] convertBaseToScreen:rowRect.origin];
    
    return rowRect;
}
/*
// This delegate method provides a transition image between the table view and the preview panel
- (id)previewPanel:(QLPreviewPanel *)panel transitionImageForPreviewItem:(id <QLPreviewItem>)item contentRect:(NSRect *)contentRect
{
    WBFSManagedGame* gameItem = (WBFSManagedGame *)item;
	
    return gameItem.image;
}
*/

@end
