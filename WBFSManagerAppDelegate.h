//
//  WBFS_File_GUIAppDelegate.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 26/01/10.
//  Copyright 2010. All rights reserved.
//

#import <Quartz/Quartz.h>
#import <Cocoa/Cocoa.h>

#import <BWToolkitFramework/BWSplitView.h>

#import "WBFSSourcesOutlineView.h"

#import "WBFSManagerWindow.h"

#define kSourcesGroupNameKey @"GroupName"
#define kSourcesGroupListKey @"GroupList"

#define kWBFSCoverViewOpenedPrefsKey @"WBFSCoverViewOpened"
#define kWBFSCoverFlowViewOpenedPrefsKey @"WBFSCoverFlowViewOpened"
#define kWBFSCoverFlowTypePrefsKey @"WBFSCoverFlowType"

#define kWBFSNumberOfCoverFlowType 2

@class WBFSDatabase, WBFSProcess, WBFSGame, WBFSActivityController, WBFSManagedObjectContext, WBFSManagedDevice, WBFSManagedGame, WBFSManagedPlace, WBFSManagedPlaylist, IKImageFlowView;
@interface WBFSManagerAppDelegate : NSObject  <NSApplicationDelegate, QLPreviewPanelDelegate, QLPreviewPanelDataSource> {	
	
    WBFSManagedObjectContext *managedObjectContext;
	
	//WBFSQueueProcessController *queueController;
	WBFSActivityController *queueController;
	
	id selectedSource;
	id dropDestination;
	NSMutableArray *selectedGames;
		
	NSMutableArray *sourcesList;
	
	BOOL coverViewOpened;
	BOOL coverFlowViewOpened;
	
	NSNumber* lastEventId;
	FSEventStreamRef stream;
	
	UInt coverFlowType;
	
	
	NSTextView *logView;
	
	NSPredicate *predicate;
	
	/* IBOutlet ivars */	
	IBOutlet NSArrayController *gamesList;
	IBOutlet NSArrayController *devicesList;
	
	IBOutlet NSView *accessoryView;
	IBOutlet NSPopUpButton *formatButton;
	IBOutlet NSPopUpButton *filenameLayoutButton;
	
	IBOutlet NSView *leftPane;
	IBOutlet NSView *rightPane;
	IBOutlet NSView *listView;
	
	IBOutlet NSView *sourcesView;
	IBOutlet NSView *coverView;
	IBOutlet NSView *gamesView;
	
	IBOutlet WBFSSourcesOutlineView *sourcesOutlineView;
	IBOutlet NSTableView *coverTableView;
	IBOutlet NSTableView *gamesTableView;
	IBOutlet IKImageFlowView *coverFlowView;
	IBOutlet NSButton *toggleCoverViewButton;
	
	IBOutlet NSSplitView *showView;
	
	IBOutlet NSSegmentedControl *coverFlowTypeControl;
	IBOutlet NSView *previewPanelView;
	IBOutlet NSImageView *previewPanelImageView;
	
	IBOutlet WBFSManagerWindow *window;
	IBOutlet NSWindow *smartPlaylistWindow;

	IBOutlet NSButton *addPlaylistButton;
}

#pragma mark -
#pragma mark IBOutlet properties
@property (nonatomic, retain) IBOutlet WBFSManagerWindow *window;
@property (nonatomic, retain) IBOutlet NSButton *addPlaylistButton;
@property (nonatomic, retain) IBOutlet IKImageFlowView *coverFlowView;
@property (nonatomic, retain) IBOutlet NSTextView *logView;
@property (nonatomic, retain) IBOutlet WBFSActivityController *queueController;

#pragma mark -
#pragma mark Read-only properties
@property (nonatomic, retain, readonly) WBFSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSArrayController *gamesList;

@property (nonatomic, retain, readonly) id selectedSource;
@property (nonatomic, retain, readonly) NSMutableArray *selectedGames;

#pragma mark -
#pragma mark Other properties
@property UInt coverFlowType;

#pragma mark -
#pragma mark Methods declaration
- (NSString *)applicationSupportDirectory;

- (void)diskDidAppeared:(NSNotification *)notification;
- (void)diskDidDisappeared:(NSNotification *)notification;
- (void)initializeEventStream;

- (void)loadPrefs;
- (void)savePrefs;
- (void)setupSourcesList;
- (void)setupGamesList;
- (void)setupCoverView;
- (void)setupCoverFlow;
- (void)setupQuickLook;
- (void)setupWindowDisplay;
- (void)updateWindowDisplay;
- (void)displayView:(NSView *)view;

- (void)addProcessActivityToQueue:(WBFSProcess *)aProcess;
- (void)refreshQueue;

- (void)openCoverViewWithAnimation:(BOOL)animate;
- (void)closeCoverViewWithAnimation:(BOOL)animate;

- (void)addPlaceWithPath:(NSString *)path;
- (void)removePlace:(WBFSManagedPlace *)place;
- (void)removePlaylist:(WBFSManagedPlaylist *)playlist;
- (void)addSource:(id)source forGroup:(NSString *)group;
- (void)removeSource:(id)source forGroup:(NSString *)group;
- (void)addGame:(WBFSGame *)game toDevice:(WBFSManagedDevice *)device;
- (void)extractGame:(WBFSGame *)game toPath:(NSString *)path withFilenameLayout:(NSString *)layout;
- (void)convertGame:(WBFSGame *)game toPath:(NSString *)path withFilenameLayout:(NSString *)layout;

- (NSPredicate *)predicateForNewSmartPlaylist;

#pragma mark -
#pragma mark IBAction Methods delaration
- (IBAction)toggleCoverView:(id)sender;
- (IBAction)togglePreviewPanel:(id)sender;
- (IBAction)toggleCoverFlowView:(id)sender;

- (IBAction)coverFlowTypeChanged:(id)sender;
- (IBAction)sourceSelected:(id)sender;
- (IBAction)gamesSelected:(id)sender;

- (IBAction)addPlace:(id)sender;
- (IBAction)addPlaylist:(id)sender;
- (IBAction)addSmartPlaylist:(id)sender;
- (IBAction)saveSmartPlaylist:(id)sender;
- (IBAction)addGames:(id)sender;
- (IBAction)deleteSelectedGames:(id)sender;
- (IBAction)convertSelectedGames:(id)sender;
- (IBAction)extractSelectedGames:(id)sender;
- (IBAction)extractAll:(id)sender;
- (IBAction)formatSelectedDevice:(id)sender;
- (IBAction)removeSelectedSource:(id)sender;
- (IBAction)reloadSelectedDeviceStatus:(id)sender;

- (IBAction)downloadAllCovers:(id)sender;

- (IBAction)test:(id)sender;

@end
