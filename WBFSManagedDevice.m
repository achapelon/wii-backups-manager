//
//  WBFSManagedDevice.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 27/03/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSManagedDevice.h"
#import "WBFSManagedObjectContext.h"
#import "WBFSProcess.h"

@implementation WBFSManagedDevice

@synthesize icon;

+ (WBFSManagedDevice *)insertWithPath:(NSString *)path forName:(NSString *)name {
	return [[[[self class] alloc] initWithPath:path forName:name] autorelease];
}

- (WBFSManagedDevice *)initWithPath:(NSString *)path forName:(NSString *)name {
	WBFSManagedObjectContext *context = [WBFSManagedObjectContext context];
	NSEntityDescription *entity = [NSEntityDescription entityForName:kDeviceEntityName inManagedObjectContext:context];
	if (self = [super initWithEntity:entity insertIntoManagedObjectContext:context]) {
		self.path = path;
		self.name = name;
		self.icon = [NSImage imageNamed:@"iconDiskSmall.png"];
	}
	
	return self;
}

- (NSString *)path {
	return [self primitiveValueForKey:kDevicePathKey];
}

- (NSString *)name {
	return [self primitiveValueForKey:kDeviceNameKey];
}

- (BOOL)formatWii {
	return [[self primitiveValueForKey:kDeviceFormatWiiKey] boolValue];
}

- (NSNumber *)totalGB {
	return [self primitiveValueForKey:kDeviceTotalGBKey];
}

- (NSNumber *)usedGB {
	return [self primitiveValueForKey:kDeviceUsedGBKey];
}

- (NSNumber *)freeGB {
	return [self primitiveValueForKey:kDeviceFreeGBKey];
}

- (void)setPath:(NSString *)path {
	[self setPrimitiveValue:path forKey:kDevicePathKey];
}

- (void)setName:(NSString *)name {
	[self setPrimitiveValue:name forKey:kDeviceNameKey];
}

- (void)setFormatWii:(BOOL)formatWii {
	[self setPrimitiveValue:[NSNumber numberWithBool:formatWii] forKey:kDeviceFormatWiiKey];
}

- (void)setTotalGB:(NSNumber *)totalGB {
	[self setPrimitiveValue:totalGB forKey:kDeviceTotalGBKey];
}

- (void)setUsedGB:(NSNumber *)usedGB {
	[self setPrimitiveValue:usedGB forKey:kDeviceUsedGBKey];
}

- (void)setFreeGB:(NSNumber *)freeGB {
	[self setPrimitiveValue:freeGB forKey:kDeviceFreeGBKey];
}

- (void)updateInfo {
	WBFSProcess *task = [[WBFSProcess alloc] init];
	[task setCommand:kWBFSProcessCommandDeviceInfo];
	[task setSource:[self path]];
	NSString *state = [task launchAndWaitUntilTermination];
	//[task release];
    
	NSScanner *scan = [NSScanner scannerWithString:state];
	float total, used, free;
	[scan scanString:@"wbfs total: " intoString:NULL];
	[scan scanFloat:&total];
	[scan scanString:@"G used: " intoString:NULL];
	[scan scanFloat:&used];
	[scan scanString:@"G free: " intoString:NULL];
	[scan scanFloat:&free];
	
	[self setTotalGB:[NSNumber numberWithFloat:total]];
	[self setUsedGB:[NSNumber numberWithFloat:used]];
	[self setFreeGB:[NSNumber numberWithFloat:free]];
}

- (void)updateInfoByUsingSpace:(float)space {
	[self setUsedGB:[NSNumber numberWithFloat:[self.usedGB floatValue]+space]];
	[self setFreeGB:[NSNumber numberWithFloat:[self.freeGB floatValue]-space]];
}

- (void)updateInfoByFreeingSpace:(float)space {
	[self setUsedGB:[NSNumber numberWithFloat:[self.usedGB floatValue]-space]];
	[self setFreeGB:[NSNumber numberWithFloat:[self.freeGB floatValue]+space]];
}


@end	
