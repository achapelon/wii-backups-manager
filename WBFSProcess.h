//
//  WBFSProcess.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 28/01/10.
//  Copyright 2010. All rights reserved.WBFSTask
//

#import <Cocoa/Cocoa.h>

/*!
 * @enum       WBFSProcessCommandsRef
 * @abstract   Commands for wbfs_file.
 */
typedef enum {
	kWBFSProcessCommandConvertRef = 0,
	kWBFSProcessCommandScrubRef,
	kWBFSProcessCommandCreateRef,
	kWBFSProcessCommandListRef,
	kWBFSProcessCommandDeviceInfo,
	kWBFSProcessCommandMakeInfoRef,
	kWBFSProcessCommandIDTitleRef,
	kWBFSProcessCommandInitRef,
	kWBFSProcessCommandAddISORef,
	kWBFSProcessCommandAddWBFSRef,
	kWBFSProcessCommandRemoveRef,
	kWBFSProcessCommandExtractISORef,
	kWBFSProcessCommandExtractWBFSRef,
	kWBFSProcessCommandExtractWBFSAllRef,
	kWBFSProcessCommandWBFSCopyRef,
	kWBFSProcessCommandListFileRef,
	kWBFSProcessCommandExtractFileRef,
	kWBFSProcessCommandDebugInfoRef,
	kWBFSProcessCommandISOInfoRef
}WBFSProcessCommandsRef;

/*!
 * @enum       WBFSProcessOptionsRef
 * @abstract   Options for wbfs_file.
 */
typedef enum {
	kWBFSProcessOptionSplitSizeRef = 0,
	kWBFSProcessOptionDontSplitSizeRef,
	kWBFSProcessOptionSplitSize2GBRef,
	kWBFSProcessOptionSplitSize4GBRef,
	kWBFSProcessOptionScrubSizeRef,
	kWBFSProcessOptionZeroFilledBlockWhenScrubbingRef,
	kWBFSProcessOptionCopyAllPartitionFromISORef,
	kWBFSProcessOptionCopyOnlyGamePartitionFromISORef,
	kWBFSProcessOptionCopyIdenticFromISORef,
	kWBFSProcessOptionForceWBFSModeRef,
	kWBFSProcessOptionTrimExtractedISOSizeRef,
	kWBFSProcessOptionLogFileCreationRef,
	kWBFSProcessOptionDestinationFilenameLayoutRef,
	kWBFSProcessOptionDestinationFilenameLayoutBRef,
	kWBFSProcessOptionDestinationFilenameLayoutDRef
}WBFSProcessOptionsRef;

/*!
 * @define     WBFSProcessCommands
 * @abstract   Commands for wbfs_file.
 */
#define kWBFSProcessCommandConvert @"convert"
#define kWBFSProcessCommandScrub @"scrub"
#define kWBFSProcessCommandCreate @"create"
#define kWBFSProcessCommandList @"ls"
#define kWBFSProcessCommandDeviceInfo @"df"
#define kWBFSProcessCommandMakeInfo @"make_info"
#define kWBFSProcessCommandIDTitle @"id_title"
#define kWBFSProcessCommandInit @"init"
#define kWBFSProcessCommandAdd @"add"
#define kWBFSProcessCommandAddISO @"add_iso"
#define kWBFSProcessCommandAddWBFS @"add_wbfs"
#define kWBFSProcessCommandRemove @"rm"
#define kWBFSProcessCommandExtractISO @"extract_iso"
#define kWBFSProcessCommandExtractWBFS @"extract_wbfs"
#define kWBFSProcessCommandExtractWBFSAll @"extract_wbfs_all"
#define kWBFSProcessCommandWBFSCopy @"wbfs_copy"
#define kWBFSProcessCommandListFile @"ls_file"
#define kWBFSProcessCommandExtractFile @"extract_file"
#define kWBFSProcessCommandDebugInfo @"debug_info"
#define kWBFSProcessCommandISOInfo @"iso_info"


/*!
 * @define     WBFSProcessOptions
 * @abstract   Options for wbfs_file.
 */
#define kWBFSProcessOptionSplitSize @"-s"
#define kWBFSProcessOptionDontSplitSize @"-0"
#define kWBFSProcessOptionSplitSize2GB @"-2"
#define kWBFSProcessOptionSplitSize4GB @"-4"
#define kWBFSProcessOptionScrubSize @"-u"
#define kWBFSProcessOptionZeroFilledBlockWhenScrubbing @"-z"
#define kWBFSProcessOptionCopyAllPartitionFromISO @"-a"
#define kWBFSProcessOptionCopyOnlyGamePartitionFromISO @"-g"
#define kWBFSProcessOptionCopyIdenticFromISO @"-1"
#define kWBFSProcessOptionForceWBFSMode @"-f"
#define kWBFSProcessOptionTrimExtractedISOSize @"-t"
#define kWBFSProcessOptionLogFileCreation @"-x"
#define kWBFSProcessOptionDestinationFilenameLayout @"-l"
#define kWBFSProcessOptionDestinationFilenameLayoutB @"-b"
#define kWBFSProcessOptionDestinationFilenameLayoutD @"-d"

@interface WBFSProcess : NSObject <NSCopying> {
	// Task vars
	NSTask *task;
	NSFileHandle *output;
	
	NSString *source;
	NSString *gameID;
	NSString *destination;
	WBFSProcessCommandsRef commandRef;
	NSString *command;
	//NSString *options;
	NSMutableDictionary *options;
	
	BOOL begun;	
	BOOL finished;	
	
	// Output vars
	NSMutableString *log;
	NSString *description;
	float progress;
	float currentSize;
	float fullSize;
	float rate;
	NSDate *startDate;
	NSDate *endDate;
	NSTimeInterval elapsedTime;
	NSTimeInterval remainingTime;
}

@property (retain) NSTask *task;
@property (retain) NSFileHandle *output;
@property (retain) NSString *source;
@property (retain) NSString *gameID;
@property (retain) NSString *destination;
@property (assign) WBFSProcessCommandsRef commandRef;
@property (retain) NSString *command;
@property (retain) NSMutableDictionary *options;

@property (retain) NSMutableString *log;
@property (assign) float progress;
@property (assign) float currentSize;
@property (assign) float fullSize;
@property (assign) float rate;
@property (assign) NSTimeInterval elapsedTime;
@property (assign) NSTimeInterval remainingTime;
@property (retain) NSDate *startDate;
@property (retain) NSDate *endDate;

- (id)initWithBinary:(NSString *)binary;

- (id)copyWithZone:(NSZone *)zone;

// Methode répondant à la notification permettant de récupérer le résultat de la commande
- (void)taskDidRefresh:(NSNotification *)notification;

- (NSString *)binary;
- (void)setOption:(NSString *)option;
- (void)setValue:(NSString *)value forOption:(NSString *)option;
// return options as an array
- (NSArray *)optionsList;
- (NSString *)commandStringByRef:(WBFSProcessCommandsRef)ref;
- (NSUInteger)commandRefByString:(NSString *)cmd;

- (void)setDestinationFilenameLayoutOption:(NSString *)layout;


- (void)launch;
- (NSString *)launchAndWaitUntilTermination;
- (BOOL)isTerminated;
- (BOOL)isRunning;

@end
