//
//  WBFSDatabase.h
//  Wii Backups Manager
//
//  Created by Anthony on 29/07/10.
//  Copyright 2010. All rights reserved.
//
//  Provide an interface to manage wiitdb.xml file

#import <Cocoa/Cocoa.h>

#define kDatabaseURLFormat		@"http://wiitdb.com/wiitdb.zip"
#define kDatabaseFilename		@"wiitdb.xml"
#define kDatabaseZipFilename	@"wiitdb.zip"

@interface WBFSDatabase : NSObject {
	NSXMLDocument *_dbDocument;
	NSMutableDictionary *_cachedGameElements;
}

@property (retain) NSXMLDocument		*database;
@property (retain) NSMutableDictionary	*cachedGameElements;

+ (id)loadDatabase;
- (void)downloadLatestDatabase;

- (NSXMLElement *)gameElementForGameID:(NSString *)gameID;

- (NSString *)stringValueForXPath:(NSString *)xpath ofGameID:(NSString *)gameID;
@end
