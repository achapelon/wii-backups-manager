/*
 *  WBFSDiskManagment.c
 *  WBFS File GUI
 *
 *  Created by Anthony Chapelon on 07/03/10.
 *  Copyright 2010. All rights reserved.
 *
 */

#include "WBFSDiskManagment.h"

#include <DiskArbitration/DASession.h>
#include <DiskArbitration/DiskArbitration.h>

// Find disks

void *WBFSGetDisks() {
	CFStringRef keys[2] = { kWBFSDiskNameKey, kWBFSDiskPathKey };
	CFStringRef values[2];
	CFMutableArrayRef disks = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);
	
	DASessionRef session = DASessionCreate(kCFAllocatorDefault);
	
	kern_return_t kr;
	io_iterator_t iter;
	kr = IOServiceGetMatchingServices(kIOMasterPortDefault, IOServiceMatching(kIOMediaClass), &iter);
	assert( KERN_SUCCESS == kr );
	
	io_object_t service;
	for (;(service = IOIteratorNext(iter));IOObjectRelease(service)) {
		io_string_t path;
		kr = IORegistryEntryGetPath(service, kIOServicePlane, path);
		
		assert( KERN_SUCCESS == kr );
		
		DADiskRef disk = DADiskCreateFromIOMedia(kCFAllocatorDefault, session, service);
		CFDictionaryRef dd = DADiskCopyDescription(disk);
		
		CFStringRef protocol = CFDictionaryGetValue(dd, kDADiskDescriptionDeviceProtocolKey);
		CFBooleanRef whole = CFDictionaryGetValue(dd, kDADiskDescriptionMediaWholeKey);
		
		if (!CFBooleanGetValue(whole) && CFEqual(protocol, kDADiskDescriptionUSBProtocolValue)) {
			CFStringRef device = (CFStringRef)CFDictionaryGetValue(dd, kDADiskDescriptionMediaBSDNameKey);	
			CFStringRef name = (CFStringRef)CFDictionaryGetValue(dd, kDADiskDescriptionVolumeNameKey);
			
			values[0] = (name == NULL) ? device : name;
			values[1] = CFStringCreateWithFormat(kCFAllocatorDefault, NULL, CFSTR("/dev/%@"), device);
			CFDictionaryRef diskDict = CFDictionaryCreate(kCFAllocatorDefault, (const void **)keys, (const void **)values, 2, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
			
			CFArrayAppendValue(disks, (const void *)diskDict);
            CFRelease(diskDict);
            CFRelease(values[1]);
			//NSLog(@"Found a disk with name %@\n", name);
		}
		CFRelease(dd);
		CFRelease(disk);
	}
	IOObjectRelease(iter);
	CFRelease(session);
	
	return disks;
}

void WBFSSetupDisksNotification() {
	DASessionRef daSession;  
    daSession = DASessionCreate(kCFAllocatorDefault);  
    DARegisterDiskAppearedCallback(daSession, NULL, WBFSDiskAppeared, NULL);      
	DARegisterDiskDisappearedCallback(daSession, NULL, WBFSDiskDisappeared, NULL);;  
    DASessionScheduleWithRunLoop(daSession, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);  
    CFRelease(daSession);
}

void WBFSDiskAppeared(DADiskRef disk, void *context) {  
    io_service_t usbDevice = DADiskCopyIOMedia(disk);  
    CFDictionaryRef dd = DADiskCopyDescription(disk);  
	CFStringRef keys[2] = { kWBFSDiskNameKey, kWBFSDiskPathKey };
	CFStringRef values[2];
	
	CFStringRef protocol = CFDictionaryGetValue(dd, kDADiskDescriptionDeviceProtocolKey);
	CFBooleanRef whole = CFDictionaryGetValue(dd, kDADiskDescriptionMediaWholeKey);
	
	if ( protocol!=nil && whole!=nil && !CFBooleanGetValue(whole) && CFEqual(protocol, kDADiskDescriptionUSBProtocolValue)) {
		CFStringRef device = (CFStringRef)CFDictionaryGetValue(dd, kDADiskDescriptionMediaBSDNameKey);	
		CFStringRef name = (CFStringRef)CFDictionaryGetValue(dd, kDADiskDescriptionVolumeNameKey);
		
		if (name == NULL) {
			values[0] = (name == NULL) ? device : name;
			values[1] = CFStringCreateWithFormat(kCFAllocatorDefault, NULL, CFSTR("/dev/%@"), device);
			CFDictionaryRef diskDict = CFDictionaryCreate(kCFAllocatorDefault, (const void **)keys, (const void **)values, 2, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);		
			
			CFNotificationCenterPostNotification(CFNotificationCenterGetLocalCenter(), CFSTR("WBFSDiskAppeared"), NULL, diskDict, true);
			
			CFRelease(diskDict);
			CFRelease(values[1]);
		}
	}  
	
	CFRelease(dd);
    IOObjectRelease(usbDevice);      
}

void WBFSDiskDisappeared(DADiskRef disk, void *context) {  
    io_service_t usbDevice = DADiskCopyIOMedia(disk);  
    CFDictionaryRef dd = DADiskCopyDescription(disk);  
	CFStringRef keys[2] = { kWBFSDiskNameKey, kWBFSDiskPathKey };
	CFStringRef values[2];
	
	CFStringRef protocol = CFDictionaryGetValue(dd, kDADiskDescriptionDeviceProtocolKey);
	CFBooleanRef whole = CFDictionaryGetValue(dd, kDADiskDescriptionMediaWholeKey);
	
	if (!CFBooleanGetValue(whole) && CFEqual(protocol, kDADiskDescriptionUSBProtocolValue)) {
		CFStringRef device = (CFStringRef)CFDictionaryGetValue(dd, kDADiskDescriptionMediaBSDNameKey);	
		CFStringRef name = (CFStringRef)CFDictionaryGetValue(dd, kDADiskDescriptionVolumeNameKey);
		
		values[0] = (name == NULL) ? device : name;
		values[1] = CFStringCreateWithFormat(kCFAllocatorDefault, NULL, CFSTR("/dev/%@"), device);
		CFDictionaryRef diskDict = CFDictionaryCreate(kCFAllocatorDefault, (const void **)keys, (const void **)values, 2, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);		
		
		CFNotificationCenterPostNotification(CFNotificationCenterGetLocalCenter(), CFSTR("WBFSDiskDisappeared"), NULL, diskDict, true);
		
		CFRelease(diskDict);
		CFRelease(values[1]);		
	}  
	
	CFRelease(dd);
    IOObjectRelease(usbDevice);      
}