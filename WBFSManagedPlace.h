//
//  WBFSManagedPlace.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 27/03/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kPlaceEntityName	@"Place"
#define kPlacePathKey		@"path"

@interface WBFSManagedPlace : NSManagedObject {
	NSImage *icon;
}

@property (nonatomic, retain) NSString *path;
@property (readonly, retain) NSString *name;
@property (nonatomic, retain) NSImage *icon;

+ (WBFSManagedPlace *)insertWithPath:(NSString *)path;
- (WBFSManagedPlace *)initWithPath:(NSString *)path;
- (WBFSManagedPlace *)insertWithPath:(NSString *)path;

@end
