//
//  WBFSProcessCell.h
//
//  Created by Anthony Chapelon on 29/01/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WBFSProcess.h"

@interface WBFSProcessCell : NSTextFieldCell {
	NSMutableDictionary *item;
	
	NSImageCell *_imageCell;
	NSTextFieldCell *_infoCell;
	NSButtonCell *_cancelButtonCell;
}

@property(retain) NSImage *image;
@property (assign) NSMutableDictionary *item;
@property (assign) NSProgressIndicator *progressIndicator;
@property (assign) WBFSProcess *process;

- (void)prepareCell;

// Initialize progress indicator to fit our case
- (NSProgressIndicator *)initProcessIndicator;
- (void)ensureProgressIndicatorCreated;

- (void)setInfo:(NSString *)info;

// Drawing methods
//- (void)drawDescWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
//- (void)drawInfoWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
//- (void)drawIconWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;
//- (void)drawIndicatorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView;

@end
