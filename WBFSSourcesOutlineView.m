//
//  WBFSSourcesOutlineView.m
//  Wii Manager
//
//  Created by Anthony on 11/06/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WBFSSourcesOutlineView.h"
#import "WBFSManagerAppDelegate.h"
#import "WBFSManagedPlace.h"
#import "WBFSManagedPlaylist.h"

@implementation WBFSSourcesOutlineView

@synthesize draggedItems;

#pragma mark -
#pragma mark NSDraggingSource Protocol

- (void)draggedImage:(NSImage *)anImage endedAt:(NSPoint)aPoint operation:(NSDragOperation)operation
{
	NSPoint	mPoint = [[self window] convertScreenToBase:[NSEvent mouseLocation]];
	NSRect viewFrame = [self frame];
	viewFrame.origin.y += 25; // Offset
	NSLog(@"draggeImage:endedAt:{%f;%f} operation:", mPoint.x, mPoint.y);
	if ( !NSMouseInRect(mPoint, viewFrame, NO) ) {
		// Is outside views bounds		
		NSShowAnimationEffect(NSAnimationEffectDisappearingItemDefault,
							  [NSEvent mouseLocation], NSZeroSize, nil, nil, nil);
		
		if ([[draggedItems objectAtIndex:0] isKindOfClass:[WBFSManagedPlace class]]) {
			for (WBFSManagedPlace *place in draggedItems) {
				[[NSApp delegate] removePlace:place];
			}
		} else {
			for (WBFSManagedPlaylist *playlist in draggedItems) {
				[[NSApp delegate] removePlaylist:playlist];
			}
		}

		
    }
}

- (void)draggedImage:(NSImage *)draggedImage movedTo:(NSPoint)aPoint {
	NSPoint mPoint = [[self window] convertScreenToBase:[NSEvent mouseLocation]];
	NSRect viewFrame = [self frame];
	viewFrame.origin.y += 25; // Offset
	NSLog(@"draggeImage:moveTo:{%f;%f}", mPoint.x, mPoint.y);
	if ( !NSMouseInRect(mPoint, viewFrame, NO) ) {
		// Is outside views bounds
		[[NSCursor disappearingItemCursor] set];
	} else {
		[[NSCursor arrowCursor] set];
	}
}

- (void)dragImage:(NSImage *)anImage at:(NSPoint)imageLoc offset:(NSSize)mouseOffset event:(NSEvent *)theEvent pasteboard:(NSPasteboard *)pboard source:(id)sourceObject slideBack:(BOOL)slideBack { 
	// Prevent slide back. 
	[super dragImage:anImage at:imageLoc offset:mouseOffset event:theEvent pasteboard:pboard source:sourceObject slideBack:NO]; 
}

- (void)keyDown:(NSEvent *)event
{	
	[super keyDown:event];
	
	switch ( [[event charactersIgnoringModifiers] characterAtIndex:0] ) {
		case NSDeleteCharacter:
			[[NSApp delegate] performSelector:@selector(removeSelectedSource:)];
			break;
		default:
			break;
	}
}

@end
