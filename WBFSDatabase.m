//
//  WBFSDatabase.m
//  Wii Backups Manager
//
//  Created by Anthony on 29/07/10.
//  Copyright 2010. All rights reserved.
//

#import "WBFSDatabase.h"

#import "WBFSManagerAppDelegate.h"

WBFSDatabase *db = nil;

@implementation WBFSDatabase

@synthesize database = _dbDocument, cachedGameElements = _cachedGameElements;

+ (id)loadDatabase {
	if (!db) {
		db = [[WBFSDatabase alloc] init];
		NSString *appSupportDir = [[NSApp delegate] applicationSupportDirectory];
		NSString *dbFile = [appSupportDir stringByAppendingPathComponent:kDatabaseFilename];
		NSURL *dbFileURL = [NSURL fileURLWithPath:dbFile];
		
		// Try to load it
		db.database = [[NSXMLDocument alloc] initWithContentsOfURL:dbFileURL options:NSXMLDocumentTidyXML error:nil];
		// If unsuccessful, download it
		if (db.database == nil) 
			[db downloadLatestDatabase];
		// Init the cached elements
		db.cachedGameElements = [[NSMutableDictionary alloc] init];
	}
	
	return db;
}

- (void)release {
	//[db.database release];
	//[db.cachedGameElements release];
	[super release];
}

- (void)downloadLatestDatabase {
	
	[[[NSURLDownload alloc] initWithRequest:
		[NSURLRequest requestWithURL: [NSURL URLWithString:kDatabaseURLFormat]] delegate:self] autorelease];
	
	/*
	NSString *appSupportDir = [[NSApp delegate] applicationSupportDirectory];
	
	// Download the zip file
	NSString *dbZipFile = [appSupportDir stringByAppendingPathComponent:kDatabaseZipFilename];
	NSURL *dbZipURL = [NSURL fileURLWithPath:dbZipFile];
	NSData *dbZipData = [NSData dataWithContentsOfURL: [NSURL URLWithString:kDatabaseURLFormat]];
	[dbZipData writeToFile: dbZipFile 
				atomically: YES];
	
	// Uncompress the downloaded zip file
	NSTask *task = [[NSTask alloc] init];
	[task setLaunchPath: @"/usr/bin/unzip"];
	[task setCurrentDirectoryPath: appSupportDir];
	[task setArguments: [NSArray arrayWithObjects:@"-o", kDatabaseZipFilename, nil]];
	[task launch];
	[task waitUntilExit];
	
	// Remove the downloaded zip file
	[[NSFileManager defaultManager] removeItemAtURL: dbZipURL 
											  error: nil];
	 */
}

- (NSXMLElement *)gameElementForGameID:(NSString *)gameID {
	NSXMLElement *rootElement = [_dbDocument rootElement];
	
	for (NSXMLElement *element in [rootElement elementsForName:@"game"]) {
		NSXMLElement *idElement = [[element elementsForName:@"id"] objectAtIndex:0];
		if ([[idElement stringValue] isEqualToString:gameID]) {
			return element;
		}
	}
	
	return nil;
}

- (NSString *)stringValueForXPath:(NSString *)xpath ofGameID:(NSString *)gameID {
	NSXMLElement *fatherElement = [_cachedGameElements valueForKey: gameID];
	if (fatherElement == nil) {
		fatherElement = [db gameElementForGameID: gameID];
		// Cache the element
		[_cachedGameElements setValue: fatherElement forKey: gameID];
	}
	
	NSArray *pathComponents = [xpath pathComponents];
	for (NSString *component in pathComponents) {
		NSArray *elementComponents = [component componentsSeparatedByString: @"."];
		NSString *elementName = [elementComponents objectAtIndex:0];
		for (NSXMLElement *element in [fatherElement elementsForName:elementName]) {
			// If no attribute fetch is found, so we found the real element
			BOOL elementFound = ([elementComponents count] == 1) ? YES : NO;
			for (NSUInteger i = 1; i < [elementComponents count]; i++) {
				NSString *attr = [elementComponents objectAtIndex:i];
				NSArray *attributeComponents = [attr componentsSeparatedByString:@"="];
				NSString *attributeName = [attributeComponents objectAtIndex:0];
				NSXMLNode *attribute = [element attributeForName:attributeName];
				// If no attribute value is found, so return the attribute value
				if ([attributeComponents count] == 1) {
					return [attribute stringValue];
				}
				NSString *attributeValue = [attributeComponents objectAtIndex:1];
				// If the attribute of the element match the passed value, so stop the loop and return the value of the matching element
				if ([[attribute stringValue] isEqualToString:attributeValue]) {
					NSUInteger idx = [pathComponents count]-1;
					for (element in [element elementsForName:[pathComponents objectAtIndex:idx]]) {
						elementFound = YES;
						break;
					}
				}
			}
			if (elementFound) {
				return [element stringValue];
			}
		}
		
	}
	
	return nil;
}

- (void)dealloc {
	[super dealloc];
}

#pragma mark -
#pragma mark NSURLDownloadDelegate

- (void)download:(NSURLDownload *)aDownload decideDestinationWithSuggestedFilename:(NSString *)filename {
    NSString *appSupportDir = [[NSApp delegate] applicationSupportDirectory];
	NSString *dbZipFile = [appSupportDir stringByAppendingPathComponent:kDatabaseZipFilename];
    [aDownload setDestination:dbZipFile allowOverwrite:YES];
}

- (void)downloadDidFinish:(NSURLDownload *)download {
	NSString *appSupportDir = [[NSApp delegate] applicationSupportDirectory];
	
	// Uncompress the downloaded zip file
	NSTask *task = [[NSTask alloc] init];	
	[task setLaunchPath: @"/usr/bin/unzip"];
	[task setCurrentDirectoryPath: appSupportDir];
	[task setArguments: [NSArray arrayWithObjects:@"-o", kDatabaseZipFilename, nil]];
	[task launch];
	[task waitUntilExit];
	[task release];
    
	// Remove the dowloaded zip file
	NSString *dbZipFile = [appSupportDir stringByAppendingPathComponent:kDatabaseZipFilename];
	NSURL *dbZipURL = [NSURL fileURLWithPath:dbZipFile];
	[[NSFileManager defaultManager] removeItemAtURL: dbZipURL 
											  error: nil];
	
	// Load the XML file in memory
	NSString *dbFile = [appSupportDir stringByAppendingPathComponent:kDatabaseFilename];
	NSURL *dbFileURL = [NSURL fileURLWithPath:dbFile];	
	db.database = [[NSXMLDocument alloc] initWithContentsOfURL:dbFileURL options:NSXMLDocumentTidyXML error:nil];
}

@end
