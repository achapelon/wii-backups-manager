//
//  WBFSManagedModelController.h
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 20/03/10.
//  Copyright 2010 . All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kGameIDPlistKey @"Game ID"
#define kTitlePlistKey @"Title"
#define kSizeGBPlistKey @"SizeGB"
#define kFormatPlistKey @"Format"
#define kPathPlistKey @"Path"
#define kRatingPlistKey @"Rating"

#define kLibraryPlistFilename @"Library.plist"
#define kRatingPlistFilename @"Rating.plist"
#define kPlacesPlistFilename @"Places.plist"
#define kPlaylistsPlistFilename @"Playlists.plist"

@class WBFSManagedDevice, WBFSManagedPlace;
@interface WBFSManagedObjectContext : NSManagedObjectContext {
	
}

+ (WBFSManagedObjectContext *)context;

- (void)fillDevicesEntity;
- (NSArray *)fillGamesEntityWithDevice:(WBFSManagedDevice *)device;
- (NSArray *)fillGamesEntityWithPlace:(WBFSManagedPlace *)place;

- (NSArray *)fetchObjectsOfEntityWithName:(NSString *)entityName andPredicate:(NSPredicate *)predicate;
- (NSArray *)fetchAllObjectsOfEntityWithName:(NSString *)entityName;

- (void)deleteObjectsOfEntityWithName:(NSString *)entityName andPredicate:(NSPredicate *)predicate;
- (void)deleteAllObjectsOfEntityWithName:(NSString *)entityName;
- (void)deleteUnknownGamesForPlace:(WBFSManagedPlace *)place;

- (NSArray *)fetchGames;
- (NSArray *)fetchGamesInLibrary;
- (NSArray *)fetchGamesForPlace:(WBFSManagedPlace *)place;
- (NSArray *)fetchPlaceForPath:(NSString *)path;
- (NSArray *)fetchDeviceForPath:(NSString *)path;
- (NSArray *)fetchDevices;
- (NSArray *)fetchPlaces;
- (NSArray *)fetchPlaylists;

- (BOOL)loadLibrary;
- (BOOL)saveLibrary;
- (BOOL)loadPlaces;
- (BOOL)savePlaces;
- (BOOL)loadPlaylists;
- (BOOL)savePlaylists;

- (BOOL)updateGamesRatingOf:(NSArray *)array;

- (void)clearCachedCoversView;

@end
