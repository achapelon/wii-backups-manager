//
//  WBFSManagedModelController.m
//  WBFS File GUI
//
//  Created by Anthony Chapelon on 20/03/10.
//  Copyright 2010. All rights reserved.
//

#include "WBFSDiskManagment.h"

#import "WBFSManagerAppDelegate.h"
#import "WBFSManagedObjectContext.h"
#import "WBFSManagedDevice.h"
#import "WBFSManagedGame.h"
#import "WBFSManagedPlace.h"
#import "WBFSManagedPlaylist.h"
#import "WBFSGame.h"
#import "WBFSProcess.h"

WBFSManagedObjectContext *managedObjectContext = nil;

@implementation WBFSManagedObjectContext

/**
 Returns the managed object context for the application (which is already
 bound to the persistent store coordinator for the application.) 
 */

+ (WBFSManagedObjectContext *)context {
    if (!managedObjectContext) {
		managedObjectContext = [[WBFSManagedObjectContext alloc] init];
		NSManagedObjectModel *mom = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
		NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: mom];
		[managedObjectContext setPersistentStoreCoordinator: coordinator];
        [mom release];
        [coordinator release];
	}	
	
	return managedObjectContext;
}

- (void)fillDevicesEntity {
	CFArrayRef disks = WBFSGetDisks();
	for (CFIndex idx=0; idx<CFArrayGetCount(disks); idx++) {
		CFDictionaryRef dict = CFArrayGetValueAtIndex(disks, idx);
		CFStringRef name = (CFStringRef)CFDictionaryGetValue(dict, kDeviceNameKey);	
		CFStringRef device = (CFStringRef)CFDictionaryGetValue(dict, kDevicePathKey);
		
		[WBFSManagedDevice insertWithPath:[NSString stringWithFormat:@"%@", device]
								  forName:[NSString stringWithFormat:@"%@", name]];
		
	}
}

- (NSArray *)fillGamesEntityWithDevice:(WBFSManagedDevice *)device {
	/*[managedObjectContext deleteObjectsOfEntityWithName:kGameEntityName 
										   andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] %@", device.path]];*/
	NSMutableArray *array = [NSMutableArray arrayWithArray:
							 [managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
																   andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] %@", device.path]]];
	
	if ([array count] == 0) {
		WBFSProcess *task = [[WBFSProcess alloc] init];
		[task setCommand:kWBFSProcessCommandList];
		[task setSource:[device valueForKey:kDevicePathKey]];
		NSString *list = [task launchAndWaitUntilTermination];
		//[task release];
		NSRange errRange = [list rangeOfString:@"Error"];
		
		BOOL error = NSLocationInRange(NSNotFound, errRange);
		if (!error) {
			NSArray *a_list = [list componentsSeparatedByString:@"\n"];
			NSUInteger i, count = [a_list count];
			for (i = 0; i < count; i++) {
				NSString *line = [a_list objectAtIndex:i];
				NSUInteger len = [line length];
				if (len > 0) {
					WBFSGame *aGame = [[WBFSGame alloc] initWithString:line];
					WBFSManagedGame *game = [WBFSManagedGame insertGame: aGame];
					[aGame release];
					game.path = [device.path stringByAppendingPathComponent:game.gameId];
					[array addObject:game];
				} else {
					break;
				}
				
			}
		}
		[device setFormatWii:!error];
		[device updateInfo];
		
		[managedObjectContext updateGamesRatingOf: array];
	}
	
	return array;
}

- (NSArray *)fillGamesEntityWithPlace:(WBFSManagedPlace *)place {
	//[managedObjectContext deleteObjectsOfEntityWithName:kGameEntityName 
	//									   andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] %@", place.path]];
	
	[managedObjectContext deleteUnknownGamesForPlace: place];
	NSArray *gamesForPlace = [managedObjectContext fetchGamesForPlace: place];
	
	NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:place.path];
	NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
	NSString *file;
	while ((file = [dirEnum nextObject])) {
		if ([[file pathExtension] isEqualToString: @"wbfs"] || [[file pathExtension] isEqualToString: @"iso"]) {
			NSString *filepath = [place.path stringByAppendingPathComponent:file];
			// Check if the game already registered
			BOOL gameFound = NO;
			WBFSManagedGame *game;
			for (game in gamesForPlace) {
				if ( [game.path isEqualToString: filepath] ) {
					gameFound = YES;
					break;
				}
			}
			// If not, insert the new game in context
			if ( !gameFound ) {
				WBFSGame *theGame = [[WBFSGame alloc] initWithFile:filepath];
				if (theGame != nil) {
					gameFound = YES;
					game = [WBFSManagedGame insertGame:theGame];
				}
				[theGame release];
			}
			if (gameFound) [array addObject:game];
		}
	}
	// Remove games not anymore in place
	NSMutableArray *noMoreGames = [NSMutableArray arrayWithArray:gamesForPlace];
	[noMoreGames removeObjectsInArray: array];
	for (WBFSManagedGame *game in noMoreGames) {
		[managedObjectContext deleteObject: game];
	}
	
	//[managedObjectContext updateGamesRatingOf: array];
	
	return array;
}

- (NSArray *)fetchGamesForPlace:(WBFSManagedPlace *)place {
	return [managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
												 andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] %@", place.path]];
	
}

- (void)deleteUnknownGamesForPlace:(WBFSManagedPlace *)place {
	[managedObjectContext deleteObjectsOfEntityWithName:kGameEntityName 
										   andPredicate:[NSPredicate predicateWithFormat:@"id = 'XXXXXX' AND path BEGINSWITH[cd] %@", place.path]];
	
}

- (NSArray *)fetchGamesInLibrary {
	NSMutableArray *games = [NSMutableArray arrayWithArray:[managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
																								 andPredicate:[NSPredicate predicateWithValue:YES]]];
	NSArray *devGames = [managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
															  andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] '/dev/'"]];
	[games removeObjectsInArray:devGames];
	
	return games;
}

- (NSArray *)fetchGames {
	NSMutableArray *games = [NSMutableArray arrayWithArray:[managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
																								 andPredicate:[NSPredicate predicateWithValue:YES]]];
		
	return games;
}

- (NSArray *)fetchDevices {
	return [managedObjectContext fetchAllObjectsOfEntityWithName:kDeviceEntityName];
}

- (NSArray *)fetchPlaces {
	return [managedObjectContext fetchAllObjectsOfEntityWithName:kPlaceEntityName];
}

- (NSArray *)fetchPlaceForPath:(NSString *)path {
	return [managedObjectContext fetchObjectsOfEntityWithName:kPlaceEntityName 
								 andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] %@", path]];
}

- (NSArray *)fetchDeviceForPath:(NSString *)path {
	return [managedObjectContext fetchObjectsOfEntityWithName:kDeviceEntityName 
												 andPredicate:[NSPredicate predicateWithFormat:@"path BEGINSWITH[cd] %@", path]];
}

- (NSArray *)fetchPlaylists {
	return [managedObjectContext fetchAllObjectsOfEntityWithName:kPlaylistEntityName];
}

- (NSArray *)fetchAllObjectsOfEntityWithName:(NSString *)entityName {
	return [managedObjectContext fetchObjectsOfEntityWithName:entityName 
												 andPredicate:[NSPredicate predicateWithValue:YES]];
}

- (NSArray *)fetchObjectsOfEntityWithName:(NSString *)entityName andPredicate:(NSPredicate *)predicate {
	NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
											  inManagedObjectContext:managedObjectContext];
	[request setEntity:entity];
	[request setPredicate:predicate];
	return [managedObjectContext executeFetchRequest:request error:nil];
}

- (void)deleteObjectsOfEntityWithName:(NSString *)entityName andPredicate:(NSPredicate *)predicate {
	NSArray *array = [managedObjectContext fetchObjectsOfEntityWithName:entityName
														   andPredicate:predicate];
	for (id object in array) {
		[managedObjectContext deleteObject:object];
		[object release];
	}
}

- (void)deleteAllObjectsOfEntityWithName:(NSString *)entityName {
	[managedObjectContext deleteObjectsOfEntityWithName:entityName 
										   andPredicate:[NSPredicate predicateWithValue:YES]];
}


- (BOOL)savePlaces {
	NSString *appSuppDir = [[NSApp delegate] applicationSupportDirectory];
	
	NSString *placesFile = [appSuppDir stringByAppendingPathComponent:kPlacesPlistFilename];
	NSMutableArray *placesArray = [NSMutableArray arrayWithCapacity:0];
	for (WBFSManagedPlace *place in [managedObjectContext fetchPlaces]) {
		[placesArray addObject:place.path];
	}
	
	return [placesArray writeToFile:placesFile atomically:YES];
}

- (BOOL)loadPlaces {
	NSString *appSuppDir = [[NSApp delegate] applicationSupportDirectory];
	
	NSString *placesFile = [appSuppDir stringByAppendingPathComponent:kPlacesPlistFilename];
	NSArray *pathArray = [NSArray arrayWithContentsOfFile:placesFile];
	for (NSString *path in pathArray) {
		[WBFSManagedPlace insertWithPath:path];
	}
	
	return YES;
}

- (BOOL)savePlaylists {
	NSString *appSuppDir = [[NSApp delegate] applicationSupportDirectory];
	
	NSString *playlistsFile = [appSuppDir stringByAppendingPathComponent:kPlaylistsPlistFilename];
	NSMutableArray *playlistsArray = [NSMutableArray arrayWithCapacity:0];
	for (WBFSManagedPlaylist *playlist in [managedObjectContext fetchPlaylists]) {
		NSMutableArray *gamesArray = [NSMutableArray array];
		for (WBFSManagedGame *game in [[playlist games] allObjects]) {
			[gamesArray addObject:game.gameId];
		}
		//playlist.predicate need to be the last because for standard playlist, it is nil
		[playlistsArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:playlist.name, @"Name", gamesArray, @"Games", playlist.predicate, @"Predicate", nil]];
	}
	
	return [playlistsArray writeToFile:playlistsFile atomically:YES];
}

- (BOOL)loadPlaylists {
	NSString *appSuppDir = [[NSApp delegate] applicationSupportDirectory];
	
	NSString *playlistsFile = [appSuppDir stringByAppendingPathComponent:kPlaylistsPlistFilename];
	NSArray *playlistsArray = [NSArray arrayWithContentsOfFile:playlistsFile];
	for (NSDictionary *playlistDict in playlistsArray) {
		WBFSManagedPlaylist *playlist = [WBFSManagedPlaylist insertWithName:[playlistDict valueForKey:@"Name"]];
		playlist.predicate = [playlistDict valueForKey:@"Predicate"];
		NSMutableSet *gamesSet = [NSMutableSet set];
		for (NSString *gameId in [playlistDict valueForKey:@"Games"]) {
			NSArray *games = [managedObjectContext fetchObjectsOfEntityWithName:kGameEntityName 
																  andPredicate:[NSPredicate predicateWithFormat:@"id == %@", gameId]];
			if ([games count] > 0) {
				[gamesSet addObject:[games objectAtIndex:0]];
			}
		}
		[playlist setGames:gamesSet];
	}
	
	return YES;
}

- (BOOL)saveLibrary {
	NSString *appSuppDir = [[NSApp delegate] applicationSupportDirectory];
	
	NSString *libraryFile = [appSuppDir stringByAppendingPathComponent:kLibraryPlistFilename];
	NSMutableArray *libraryArray = [NSMutableArray arrayWithCapacity:0];
	NSString *ratingFile = [appSuppDir stringByAppendingPathComponent:kRatingPlistFilename];
	NSMutableArray *ratingArray = [NSMutableArray arrayWithContentsOfFile:ratingFile];
	if (!ratingArray) {
		ratingArray = [NSMutableArray arrayWithCapacity:0];
	}
	
	// Save Library
	NSArray *library = [managedObjectContext fetchGamesInLibrary];
	for (WBFSManagedGame *game in library) {
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:game.gameId, kGameIDPlistKey,
																		game.name, kTitlePlistKey,
																		game.sizeGB, kSizeGBPlistKey,
																		game.format, kFormatPlistKey,
																		game.path, kPathPlistKey, nil];
		[libraryArray addObject:dict];
	}
	
	// Save Rating
	NSArray *all = [managedObjectContext fetchGames];
	for (WBFSManagedGame *game in all) {
		if (game.rating > 0) {
			for (NSDictionary *ratingDict in ratingArray) {
				if ([[ratingDict objectForKey:kGameIDPlistKey] isEqualToString:game.gameId]) {
					[ratingArray removeObject:ratingDict];
					break;
				}
			}
			NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:game.gameId, kGameIDPlistKey,
								  game.rating, kRatingPlistKey, nil];
			[ratingArray addObject:dict];
		}
	}
	
	return ([ratingArray writeToFile:ratingFile atomically:YES] && [libraryArray writeToFile:libraryFile atomically:YES]);
}

- (BOOL)loadLibrary {
	NSString *appSuppDir = [[NSApp delegate] applicationSupportDirectory];
	
	NSString *libraryFile = [appSuppDir stringByAppendingPathComponent:kLibraryPlistFilename];
	NSArray *library = [NSArray arrayWithContentsOfFile:libraryFile];
	
	for (NSDictionary *dict in library) {
		WBFSGame *aGame = [[WBFSGame alloc] initWithString: 
						   [NSString stringWithFormat: @"%@ : %@ %.2fG", 
							[dict objectForKey:kGameIDPlistKey],
							[dict objectForKey:kTitlePlistKey],
							[[dict objectForKey:kSizeGBPlistKey] floatValue]]];
		WBFSManagedGame *game = [WBFSManagedGame insertGame: aGame];
		game.format = [dict objectForKey:kFormatPlistKey];
		game.path = [dict objectForKey:kPathPlistKey];
		game.rating = [dict objectForKey:kRatingPlistKey]; // For compatibility with old version
		
		[aGame release];
	}
	[managedObjectContext updateGamesRatingOf: [managedObjectContext fetchGames]];
	
	return YES;
}

- (BOOL)updateGamesRatingOf:(NSArray *)array {
	NSString *appSuppDir = [[NSApp delegate] applicationSupportDirectory];
	
	NSString *ratingFile = [appSuppDir stringByAppendingPathComponent:kRatingPlistFilename];
	NSArray *rating = [NSArray arrayWithContentsOfFile:ratingFile];
	
	for (WBFSManagedGame *game in array) {
		NSNumber *r = game.rating;
		for (NSDictionary *ratingDict in rating) {
			if ([[ratingDict objectForKey:kGameIDPlistKey] isEqualToString:game.gameId]) {
				r = [ratingDict objectForKey:kRatingPlistKey];
			}
		}
		game.rating = r;
	}
	
	return YES;
}

/**
 Implementation of dealloc, to release the retained variables.
 */
- (void)dealloc {
	[managedObjectContext release];
	
	[super dealloc];
}

- (void)clearCachedCoversView {
	NSArray *games = [managedObjectContext fetchGames];
	for (WBFSManagedGame *game in games) {
		[game clearCachedImage];
	}
}

@end
